$(function () {
  $(".main-slider").slick({
    autoplay: false,
    arrows: false,
    fade: true,
    asNavFor: ".thumbnail-slider",
    speed: 100,
  });
  $(".thumbnail-slider").slick({
    slidesToShow: 3,
    asNavFor: ".main-slider",
    infinite: false,
    focusOnSelect: true,
    arrows:false,
    speed: 100,
  });
});
