<?php
$setPath= "../";
$pageTitle = "問い合わせフォーム";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "スズキアリーナ茅ヶ崎の問い合わせフォームのページです。",
);
?>
<?php
// session始める - 見ればわかるか
session_start();
// ライブラリのcommonを呼び出す - 全フォーム共通
require_once(dirname(__FILE__)."/lib/Mulgu/controller/Common.php");
// ライブラリのcontactを呼び出す - 対象フォーム用
require_once(dirname(__FILE__)."/lib/Mulgu/controller/Contact.php");
// common オブジェクト
$common = new Common();
// contact オブジェクト - パラメーター収めたりする
$contact = new Contact();
$obj = $contact->form();
$params = $obj->params;
if ( $obj->has_error ) {
  $errors = $obj->errors;
}
?>

<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
</head>

<body class="contact">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">お問い合わせ</span><br><span class="en">CONTACT</span></h2>
    </div>
  </section>

  <section class="con01">
    <div class="wrapper">
      <h3 class="tit01">お電話でのお問い合わせ</h3>
      <div class="tel_info">
        <p class="number"><span><a href="tel:0467-87-1883">0467-87-1883</a></span></p>
        <p class="txt">営業時間：9:00〜18:00　<br class="sp-on">定休日：水曜日</p>
      </div>

      <h3 class="tit01">メールでのお問い合わせ</h3>

      <form class="contact-form form -input" name="contact-form" id="contact-form" action="<?php echo $contact->path["confirm"]; ?>" method="POST">
      <input type="hidden" name="token" value="<?php echo $common->mlg->csrf->generate()?>">

        <p><span class="kome">＊</span>は入力必須項目です。</p>

        <table>
          <tr class="contact-table-wrap<?php if ( $errors['name'] ) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -required required"><span>お名前または会社名</span></th>
            <td class="contact-table-cont"><input type="text" name="name" id="name" value="<?php echo $params['name']; ?>" class="-input -text txt02">
<?php if( $errors['name'] ){ ?>
                <p class="-error-message">
                  <?php echo $errors['name']; ?>
                </p>
<?php } ?>
          </td>
          </tr>


          <tr class="contact-table-wrap<?php if ( $errors['email'] ) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -required required"><span>メールアドレス</span></th>
            <td class="contact-table-cont"><input type="text" name="email" id="email" value="<?php echo $params['email']; ?>" class="-input -text txt02">
<?php if( $errors['email'] ){ ?>
                <p class="-error-message">
                  <?php echo $errors['email']; ?>
                </p>
<?php } ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>メールアドレス（確認用）</span></th>
            <td class="contact-table-cont">
            <input class="-input txt02" name="email_double" type="text" value="<?php echo $params['email_double']; ?>" style="ime-mode:disabled;">
<?php if( $errors['email_double'] ){ ?>
                <p class="-error-message">
                  <?php echo $errors['email_double']; ?>
                </p>
<?php } ?>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ( $errors['contact_type'] ) { ?> -error<?php } ?>">
            <th class="contact-table-ttl">お問い合わせ種別</th>
            <td class="contact-table-cont contact_radio">
              <?php echo $contact->mlg->tools->make_radio_form($params, $contact->mlg->master->contact_type, 'contact_type', "text"); ?>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ( $errors['comment'] ) { ?> -error<?php } ?>">
            <th class="required"><span>お問い合わせ内容</span></th>
            <td>
              <textarea name="comment" class="-textarea"><?php echo $params['comment']; ?></textarea>
<?php if( $errors['comment'] ){ ?>
                <p class="-error-message">
                  <?php echo $errors['comment']; ?>
                </p>
<?php } ?>
            </td>
          </tr>
        </table>

        <div class="agree">
          <p>「<a href="../privacypolicy/" target="_blank">プライバシーポリシー</a>」をご一読の上、内容に同意いただける場合は「同意する」にチェックし、内容確認画面にお進みください。</p>
          <div class="check">
            <label><input type="checkbox" name="is_checked" class="-checked js-consent" id="-checked" value="t">同意する</label>
          </div>
        </div>
        <div class="contact-btn">
          <a href="javascript:void(0)" onclick="do_submit(this,'contact-form');return false;" class="contact-btn-item -btn01 -stop js-confirm">確認画面へ</a>
        </div>
      </form>
    </div>
  </section>
  
  <?php require_once($setPath.'lib/include/footer.php'); ?>

  <script>
    // 同意チェック
    function do_consent() {
      if (jQuery(".js-consent:checked").val()) {
        jQuery(".js-confirm").removeClass("-stop");
      } else {
        jQuery(".js-confirm").addClass("-stop");
      }
    }

    jQuery('.js-consent').on('load click', function() {
      do_consent();
    });

    jQuery('.js-confirm').on('click', function() {
      do_submit(this,'contact_form');
    });
    
    // 問い合わせ
    function do_submit(a,fname) {
      if ( jQuery(".js-consent:checked").val() ) {
        document[fname].submit();
        return false;
      }
    }

    // 同意チェック
    function do_consent() {
      if ( jQuery(".js-consent:checked").val() ) {
        jQuery(".js-confirm").removeClass("-stop");
      }else{
        jQuery(".js-confirm").addClass("-stop");
      }
    }
    
  </script>
</body>
</html>
