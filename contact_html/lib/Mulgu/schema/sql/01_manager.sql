
CREATE TABLE `manager` (
    `id`              int(11) NOT NULL auto_increment,
    `regist_time`     timestamp default current_timestamp(),
    `modify_time`     timestamp DEFAULT 0,
    `delete_flag`     tinyint(1) NOT NULL DEFAULT '0',

    `name`            text,
    `login_id`        text NOT NULL,
    `passwd`          text NOT NULL,

    `expire_time`     timestamp NOT NULL,

    PRIMARY KEY  (`id`)
) ENGINE = MyISAM AUTO_INCREMENT = 10000 DEFAULT CHARSET = utf8;

CREATE INDEX manager_login_id ON manager(login_id(50));

