<?php
/* 諸々の設定を書いていくファイル */

define('IS_DEVELOP', 'FALSE'); // TRUE / FALSE


// 基本設定
define('PROJECT_NAME',  'MULGU');

define('PROJECT_ROOT',  '/home/kschigasaki/arena-chigasaki.jp/public_html/test.arena-chigasaki.jp/contact/lib/Mulgu');  // Mulgu 本体のディレクトリまでのパス
define('DOCUMENT_ROOT', '/home/kschigasaki/arena-chigasaki.jp/public_html/test.arena-chigasaki.jp/contact/');

define('PREFIX',        '');                          // SSL が使えるなら TRUE に
define('DOMAIN',        '');       // ドメイン
define('DOMAIN_SSL',    '');       // ドメイン
define('ENABLE_SSL',    'FALSE');                     // SSL が使えるなら TRUE に

// パスワード系
define('SHA_PASSCODE', "nWsa$10s/YsX");
define('SHA_TYPE', "sha256");

// 各フォルダの場所
define('SAVED_URL',     "saved/");
define('TMP_DIR',       PROJECT_ROOT . "/tmp/");
define('TMP_URL',       "tmp/");
define('MASTER_DIR',    PROJECT_ROOT . "/master/");
define('LOG_DIR',       PROJECT_ROOT . "/log/");
define('SAVED_DIR',     PROJECT_ROOT . "/saved/");
define('SCHEMA_DIR',    PROJECT_ROOT . "/schema/");

// wordpressの設定
// define('USE_WP',  'true');   // true / false
// define('WP_DIR',  DOCUMENT_ROOT . "/wp/wp");
// define('WP_LOAD', WP_DIR . "/wp-blog-header.php");

// mysql 設定
// WP の読み込みを阻害するので一旦コメントに
// define('DB_HOST',       '');
// define('DB_NAME',       '');
// define('DB_USER',       '');
// define('DB_PASSWD',     '');

// メールの設定
define('CONTACT_TO', 'arenachigasaki@gmail.com');   // contact のメール飛び先アドレス
define('CONTACT_BCC', 'kurihara-a@cccsac.com,okamoto-h@cccsac.com,igo@cccsac.com,takumibanno1018@gmail.com,hirao.emi1120@gmail.com,terasawa515@gmail.com');   // bccアドレス
define('CONTACT_FROM', 'arenachigasaki@gmail.com');   // contact の表示アドレス

?>
