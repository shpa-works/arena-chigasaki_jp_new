<?php
    // config を読み込むクラス
    require_once(dirname(__FILE__)."/../Band/Spyc.php");
    require_once(dirname(__FILE__)."/../../config/settings.php");

    class Config {
        public function __construct() {

            $this->is_develop = IS_DEVELOP;
            if ( $this->is_develop == "TRUE" ) {
                ini_set("display_errors", 1);
                error_reporting(E_ALL & ~E_NOTICE);
            }

            $this->project_name = PROJECT_NAME;
            $this->project_root = PROJECT_ROOT;
            $this->document_root = DOCUMENT_ROOT;
            $this->url_http = "http://" . DOMAIN . "";
            $this->url_https = ENABLE_SSL ? "https://" . DOMAIN_SSL . "/" : "http://" . DOMAIN . "/";
            $this->rel_base = PREFIX;

            $this->log_dir = LOG_DIR;
            $this->master_dir = MASTER_DIR;
            $this->saved_dir = SAVED_DIR;
            $this->saved_url = SAVED_URL;
            $this->tmp_dir = TMP_DIR;
            $this->tmp_url = TMP_URL;
            $this->schema_dir = SCHEMA_DIR;

            $this->sha_passcode = SHA_PASSCODE;
            $this->sha_type = SHA_TYPE;

            $this->db_info = Array(
                "db_host"       => DB_HOST,
                "db_user"       => DB_USER,
                "db_passwd"     => DB_PASSWD,
                "db_name"       => DB_NAME,
            );

            $this->wp_load = WP_LOAD;

            // テーブルの読み込み
            $this->parseTable();

        }

        public function parseTable () {
            // まずはファイルをスキャン
            $files = scandir( $this->schema_dir );
            $this->table = new stdClass;
            // ファイルごとにパース
            foreach ( $files as $file ) {
                // yaml のみ得る
                if ( preg_match("/^(.*)\.yml$/", $file, $match) ) {
                    $name = $match[1];
                    $obj = Spyc::YAMLLoad($this->schema_dir . $name . ".yml");
                    $this->table->$name = $obj;
                }
            }
        }
    }

?>