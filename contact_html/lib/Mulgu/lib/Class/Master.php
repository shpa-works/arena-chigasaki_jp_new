<?php
    require_once(dirname(__FILE__)."/../Band/Spyc.php");
    require_once(dirname(__FILE__)."/../Class/Config.php");

    // master
    class Master {

        // ファイルを読み込めるようにする
        public function __construct() {
            $this->parse();
        }

        // YAMLをパースする
        public function parse() {
            $config = new Config();
            // まずはファイルをスキャン
            $master_dir = $config->master_dir;
            $files = scandir( $config->master_dir );

            // ファイルごとにパース
            foreach ( $files as $file ) {
                // yaml のみ得る
                if ( preg_match("/^(.*)\.yml$/", $file, $match) ) {
                    $name = $match[1];
                    $obj = Spyc::YAMLLoad($master_dir . $name . ".yml");
                    $this->$name = $obj;
                }
            }
        }

        // YAMLをパースする
        public function parse_single($name) {
            if ( ! $name ) {
                return Array();
            }
            $config = new Config();
            // まずはファイルをスキャン
            $master_dir = $config->master_dir;
            $files = scandir( $config->master_dir );

            // ファイルごとにパース
            $obj = Spyc::YAMLLoad($master_dir . $name . ".yml");
            return $obj;
        }

        public function id_to_name($name, $id) {
            $config = new Config();
            // パース
            $list = $this->parse_single($name);
            foreach ( $list as $p ) {
                if ( $p["id"] == $id ) {
                    return $p["name"];
                }
            }
        }

        public function key_to_name($name, $key) {
            $config = new Config();
            // パース
            $list = $this->parse_single($name);
            foreach ( $list as $p ) {
                if ( $p["key"] == $key ) {
                    return $p["name"];
                }
            }
        }

    }

?>