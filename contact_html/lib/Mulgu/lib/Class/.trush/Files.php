<?php
    require_once(dirname(__FILE__)."/../Band/Upload/class.upload.php");
    require_once(dirname(__FILE__)."/../Class/Config.php");

    // upload
    class Files {

        public function save($name, $dir) {
            $config = new Config();
            $upload_dir = $config->saved_dir . $dir . "/";
            $save_url = $config->saved_url . $dir . "/";

            $file_name = $this->makeRandStr(20);

            // 読み込み
            $handle = new Upload($_FILES[$name]);

            // 実行
            if ($handle->uploaded) {
                // Lサイズ画像
                $handle->allowed = array('image/*'); // 画像のみ許可
                $handle->image_convert = 'jpg'; // jpgに変換
                $handle->file_overwrite = false; // 上書き許可
                $handle->file_auto_rename = false; // 自動リネーム禁止
                $handle->file_src_name_body = $file_name; // ファイル名
                $handle->image_resize = true; // リサイズ許可
                $handle->image_ratio = true; // 縦横比維持
                $handle->image_x = 800; // 横最大値
                $handle->image_y = 800; // 縦最大値
                $handle->image_ratio_no_zoom_in = false; // image_x、image_yより小さいサイズは拡大禁止
                $handle->Process($upload_dir);
                if ($handle->processed) {
                    $handle->clean();
                } else {
                    echo 'error : ' . $handle->error;
                }
                return $save_url . $file_name . ".jpg";
            } else {
                 return $handle->error;
            }
        }

        /**
         * ランダム文字列生成 (英数字)
         * $length: 生成する文字数
         */
        public function makeRandStr($length) {
            $str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'));
            $r_str = null;
            for ($i = 0; $i < $length; $i++) {
                $r_str .= $str[rand(0, count($str))];
            }
            return $r_str;
        }
    }


?>