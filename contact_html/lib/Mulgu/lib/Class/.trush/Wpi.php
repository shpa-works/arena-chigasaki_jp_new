<?php
    require_once(dirname(__FILE__)."/../Band/Spyc.php");
    require_once(dirname(__FILE__)."/Config.php");

    class Wpi {
        public function __construct() {
            $config = new Config();
            require_once($config->wp_load);
        }

        /*
        * $query = Array(
        *     'limit'       => (int) $limit,
        *     'category_id' => (int) $category_id,
        *     'sort_order'  => (string) $sort_order,
        *     'sort_asc'    => (DESC|ASC),
        *     'label'       => (string) $label // カスタム投稿タイプの場合はラベルを指定
        *     'args'        => Array(
        *                         "hoge" => "fuga",
        *                      ), // ここに入っているものは、そのまま通す
        * );
        */

        public function search($query) {

            $res = new stdClass;
            $q['paged'] = $query['page'] ?: 1;
            if ( $query['limit'] ) {
              $q['posts_per_page'] = $query['limit'];
            }
            if ( $query['category_id'] ) {
              $q['category__in'] = $query['category_id'];
            }
            if ( $query['sort_order'] ) {
              $q['orderby'] = $query['sort_order'];
            }
            if ( $query['sort_asc'] ) {
              $q['order'] = $query['sort_asc'] == "DESC" ? "DESC" : "ASC";
            }
            if ( $query['label'] ) {
              $q['post_type'] = $query['label'];
            }
            if ( $query['tax'] ) {
              $q['tax_query'] = $query['tax'];
            }
            if ( $query['taxonomy'] ) {
              $taxonomy = $query['taxonomy'];
            }
            if ( is_array($query['args']) ) {
              foreach ( $query['args'] as $key => $val ) {
                $q[$key] = $val;
              }
            }
            $mres = new WP_Query($q);

            if ( $mres->posts ) {
              // meta をつける
              $posts = $this->__add_post_meta($mres->posts, $taxonomy);
              $res->list = $posts;
              $res->pager = $this->pager($mres->found_posts, $query['limit'], $query['page']);
              $res->is_success = 1;
            } else {
              $res->is_success = '';
              $res->error = '記事が見つかりませんでした';
            }
            return $res;
        }

        public function find($query) {
            $res = new stdClass;
            if ( $query['id'] ) {
                $q['p'] = $query['id'];
            }
            if ( $query['label'] ) {
              $q['post_type'] = $query['label'];
            }
            if ( $query['post_status'] ) {
              $q['post_status'] = $query['post_status'];
            }
            if ( $query['taxonomy'] ) {
              $taxonomy = $query['taxonomy'];
            }

            $mres = new WP_Query($q);
            if ( $mres->posts ) {
              // meta をつける
              $posts = $this->__add_post_meta($mres->posts, $taxonomy);
              $res->post = $posts[0];
              $res->is_success = 1;
            } else {
              $res->is_success = '';
              $res->error = '記事が見つかりませんでした';
            }
            return $res;
        }

        public function get_taxonomy_list($label, $taxonomy, $term) {
          $list = get_categories( array('post_type' => $label, 'taxonomy' => $taxonomy ) );
          // 親を探す
          $parent = '';
          foreach ( $list as $data ) {
            if ( $data->slug == $term ) {
              $parent = $data;
            }
          }
          if ( $parent == '' ) {
            return Array();
          }
          $l = Array();
          foreach ( $list as $data ) {
            if ( $parent->term_id == $data->parent ) {
              $l[] = $data;
            }
          }
          return $l;
        }

        // カスタムフィールドの値を補完する
        public function __add_post_meta($posts, $taxonomy = null) {
          $list = Array();
          foreach ( $posts as $post ) {
            $post_meta = get_post_custom( $post->ID );
            foreach ($post_meta as $key => $val) {
                if ( ! preg_match("/^_/", $key) ) {
                    if ( preg_match("/image/", $key) ) {
                        $image = wp_get_attachment_image_url($val[0], "full");
                        $post->$key = $image;
                    } else {
                        $post->$key = $val[0];
                    }
                }
            }
            // カテゴリの処理
            $post_category = get_the_category($post->ID);
            foreach ($post_category as $val) {
                $post->slug = $val->slug;
            }
            // termsの処理
            if ( $taxonomy ) {
              $post_terms = get_the_terms($post->ID, $taxonomy);
              $terms = Array();
              foreach ($post_terms as $val) {
                if ( $val->parent !== 0 ) {
                  $terms[] = $val;
                }
              }
              $post->taxonomy = $terms;
            }
            // 日付の処理
            $post->post_date_ymd = date('Y/m/d', strtotime($post->post_date));
            $list[] = $post;
          }
          return $list;
        }

        public function pager ( $total, $limit, $page = 1 ) {
            $pager = new stdClass;
            if ( $total && $limit ) {
                $pager->offset       = $offset = ( $page - 1 ) * $limit;
                $pager->page_total   = ceil( $total / $limit );
                $pager->total        = $total;
                $pager->limit        = $limit;
                $pager->page         = $page;
                $pager->page_start   = $page * $limit - $limit + 1;
                $pager->page_end     = ( $page == $pager->page_total ) ? ($total % $limit) + $page->total + $limit : ( $page * $limit ) ;
                $pager->next         = ( $page == $pager->page_total ) ? null : $page + 1;
                $pager->prev         = ( $page == 1 ) ? null : $page - 1;
                return $pager;
            }
        }
    }


?>