<?php
    require_once(dirname(__FILE__)."/../Band/Spyc.php");
    require_once(dirname(__FILE__)."/Config.php");

    class Tools {
        public function timestr($string, $format = "Y/m/d H:i") {
            date_default_timezone_set('Asia/Tokyo');
            $myDate = strtotime($string);
            return date('Y/m/d H:i', $myDate);
        }

        public function comma($number) {
            return number_format($number);
        }

        // パラメータの取得系
        public function params() {
            // POST か GET か判断する
            $params;
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $params = $_POST;
            } else {
                $params = $_GET;
            }
            // カナの処理をする
            //$params_k = kana_change($params);

            // htmlspecialchars をかけながら返す
            return h($params);
        }


        // yaml から設定をとってくる
        public function getConfig($table_name) {
            if ( ! $table_name ){
                return;
            }

            $config = new Config();
            // 取得する
            $obj = Spyc::YAMLLoad($config->schema_dir . $table_name . ".yml");
            return $obj;
        }


        // pagenation の作成
        public function pagenation( $pager, $show_page = 5, $path = "?page=" ) {
        }

        public function add_session( $data, $prefix ) {
            $_SESSION[$prefix] = $data;
        }
        public function get_session( $params, $prefix ) {
            if ( $_SESSION[$prefix] ) {
                foreach ( $_SESSION[$prefix] as $key => $val ) {
                    if ( ! $params[$key] ) {
                        $params[$key] = $val;
                    }
                }
            }
            return $params;
        }
        public function delete_session( $prefix ) {
            $_SESSION[$prefix] = Array();
        }
        public function is_check ( $params, $prefix ) {
            $is_check = 0;
            if ( $params["is_check"] ) {
                $is_check = 1;
            }
            if ( $_SESSION[$prefix]["is_check"] ) {
                $is_check = 1;
            }
            return $is_check;
        }

        // dump
        public function dumper($string, $die = 0) {
            echo "<pre>";
            print_r($string);
            echo "</pre>";
            if ( $die ) { die; }
        }
        public function braw( $string ) {
            return nl2br($string);
        }

        public function params_to_string() {
            $params = $this->params();
            $p = Array();
            foreach( $params as $key => $value ) {
                if ( is_array($value) ) {
                    foreach( $value as $v ) {
                    array_push($p, $key . "[]=" . $v );
                    }
                } else {
                    array_push($p, $key . "=" . $value );
                }
            }
            return implode("&", $p);
        }

        public function params_to_hidden() {
            $params = $this->params();
            $p = Array();
            foreach( $params as $key => $value ) {
                if ( $key != "is_check" ) {
                    if ( is_array($value) ) {
                        foreach( $value as $v ) {
                            preg_replace("/\"/", "”", $v);
                            array_push($p, '<input type="hidden" name="'.$key.'[]" value="'.$v.'">');
                        }
                    } else {
                        preg_replace("/\"/", "”", $value);
                        array_push($p, '<input type="hidden" name="'.$key.'" value="'.$value.'">');
                    }
                }
            }
            return implode("\n", $p)."\n";
        }

        public function get_select_form($val, $data) {
            return $data[$val];
        }
        //radio生成関数
        public function make_select_form($params, $data, $name, $type = "integer", $default = '1', $def = '選択してください'){
            $count = 0;
            $body = '<option value="">'.$def.'</option>';
            foreach($data as $val){
                if ( $type == "text" ) {
                    $str = '';
                    if ( $params[$name] ) {
                        if ( $val["name"] == $params[$name] ) {
                            $str = ' selected="selected"';
                        }
                    } else {
                        if ( $count == $default - 1 ) {
                            // $str = ' selected="selected"';
                        }
                    }
                    $body .= sprintf('<option value="%s"%s>%s</option>', $val["name"], $str, $val["name"]);
                } else if ( $type == "integer" ) {
                    $str = ($params[$val["name"]] && $count == $params[$val["name"]]) ? ' selected="selected"' : '';
                    $body .= sprintf('<option value="%s"%s>%s</option>', $val["id"], $str, $val["name"]);
                }
                $count++;
            }
            return sprintf('<select id="%s" name="%s" class="-select -%s">%s</select>', $name,$name, $name, $body);
        }


        //radio生成関数
        public function make_radio_form($params, $data, $name, $type = "integer", $default = '1'){
            $count = 0;
            foreach($data as $val){
                if ( $type == "text" ) {
                    $str = '';
                    if ( $params[$name] ) {
                        // foreach ( $params[$name] as $k => $v ) {
                            if ( $val["name"] == $params[$name] ) {
                                $str = ' checked="checked"';
                            }
                        // }
                    } else {
                        if ( $count == $default - 1 ) {
                            $str = ' checked="checked"';
                        }
                    }
                    $body .= "<li class=\"-radio-list-item\"><label for=\"". $name . $val["id"] ."\"><input id=\"". $name . $val["id"] ."\" type=\"radio\" class=\"-radio\" name=\"{$name}\" value=\"{$val["name"]}\"{$str}>&nbsp;{$val["name"]}</label></li>\n";
                } else if ( $type == "integer" ) {
                    $str = ($params[$val["name"]] && $count == $params[$val["name"]])?' checked="checked"':'';
                    $body .= "<li class=\"-radio-list-item\"><label for=\"". $name . $val["id"] ."\"><input id=\"". $name . $val["id"] ."\" type=\"radio\" class=\"-radio\" name=\"{$name}\" value=\"{$val["id"]}\"{$str}>&nbsp;{$val["name"]}</label></li>\n";
                }
                $count++;
            }
           return "<ul class=\"-radio-list\">".$body."</ul>";
        }
        //checkbox生成関数
        public function make_check_form($params, $data, $name, $type = "integer"){
            $count = 0;
            foreach($data as $val){
                if ( $type == "text" ) {
                    $str = '';
                    if ($params[$name]){
                        foreach ( $params[$name] as $k => $v ) {
                            if ( $val["name"] == $v ) {
                                $str = ' checked="checked"';
                            }
                        }
                    }
                    $body .= "<li class=\"-checkbox-list-item\"><label for=\"". $name . $val["id"] ."\"><input id=\"". $name . $val["id"] ."\" type=\"checkbox\" class=\"-checkbox\" name=\"{$name}[]\" value=\"{$val["name"]}\"{$str}>&nbsp;{$val["name"]}</label></li>\n";
                } else if ( $type == "integer" ) {
                    $str = ($params[$name] && in_array($val["id"], $params[$name]))?' checked="checked"':'';
                    $body .= "<li class=\"-checkbox-list-item\"><label for=\"". $name . $val["id"] ."\"><input id=\"". $name . $val["id"] ."\" type=\"checkbox\" class=\"-checkbox\" name=\"{$name}[]\" value=\"{$val["id"]}\"{$str}>&nbsp;{$val["name"]}</label></li>\n";
                }
                $count++;
            }
            return "<ul class=\"-checkbox-list\">".$body."</ul>";
        }

        // 乱数を作成(連投阻止用)
        public function randNumber() {
            return rand(0, 9999);
        }

    }

function h($str) {
    if (is_array($str)) {
        return array_map("h", $str);
    } else {
        return htmlspecialchars($str, ENT_QUOTES);
    }
}


?>