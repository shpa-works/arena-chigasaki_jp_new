<?php
    require_once(dirname(__FILE__)."/Config.php");

    // DB に接続したり
    // DB から読み書きしたりする Class

    // schema
    class Schema {
        public function __construct() {
            $this->config = new Config();
            $db_info = $this->config->db_info;
            $this->db_host   = $db_info["db_host"];
            $this->db_user   = $db_info["db_user"];
            $this->db_passwd = $db_info["db_passwd"];
            $this->db_name   = $db_info["db_name"];
        }

        // connect
        public function connect() {
            $mysqli = new mysqli(
                $this->db_host,
                $this->db_user,
                $this->db_passwd,
                $this->db_name
            );

            // connect のチェック
            if ( ! $mysqli->connect_error) {
                $mysqli->set_charset("utf8");
            }
            //エラーが発生したら
            if ($mysqli->connect_error){
              print("接続失敗：" . $mysqli->connect_error);
              exit();
            }
            return $mysqli;
        }

        public function close($mysqli) {
            $mysqli->close();
        }

        // insert する処理
        public function insert( $params, $table ) {
            $res = new stdClass;
            // 最初は必ず connect
            $mysqli = $this->connect();

            // sql を作る
            // エスケープをがんばろう
            $columns = Array();
            $queries = Array();

            foreach ( $params as $key => $val ) {
                array_push($columns, $key);
                array_push($queries, $mysqli->escape_string($val));
            }

            $sql = "INSERT INTO ". $table . " ( " . implode(", ", $columns) ." ) VALUES ( '" . implode("', '", $queries) . "' )";

            if( $mysqli->query( $sql ) ) {
                $res->is_success = 1;
            } else {
                $res->error = $mysqli->error;
            }

            // 最後は close
            $close = $this->close($mysqli);

            return $res;
        }

        // update する処理
        public function update( $params, $table, $id ) {
            $res = new stdClass();
            if ( ! $id ) {
                $res->error = "id is null";
                return $res;
            }

            // 最初は必ず connect
            $mysqli = $this->connect();

            // sql を作る
            // エスケープをがんばろう
            $columns = Array();

            foreach ( $params as $key => $val ) {
                array_push($columns, $key . " = '" . $mysqli->escape_string($val) . "'" );
            }

            if ( count($columns) == 0 ) {
                $res->error = "null update columns";
                return $res;
            }

            $sql = "UPDATE ".$table." SET ". implode(", ", $columns) ." WHERE id = " . $id;

            if( $mysqli->query( $sql ) ) {
                $res->is_success = 1;
            } else {
                $res->error = $mysqli->error;
            }

            // 最後は close
            $close = $this->close($mysqli);

            return $res;
        }

        // page を処理する
        public function ofs_limit ( $page, $limit, $total ) {
            if ( ! $page || ! $limit ) {
                return;
            }
            $page_real = ceil($total / $limit);

            // page が page_real より大きければ page_real の方を採用
            if ( $page > $page_real ) {
                $page = $page_real;
            }
            $offset = ( $page - 1 ) * $limit;
            if ( $offset < 0 ) {
                $offset = 0;
            }

            $sql = "LIMIT " . $limit . " OFFSET " . $offset;

            return $sql;
        }


        // find する処理
        // select で該当の一件を取得
        public function find($table, $id, $id_name = "id", $flag_name = "delete_flag") {
            $res = new stdClass();
            if ( ! $id ) {
                $res->error = "id is null";
                return $res;
            }
            // connect する
            $mysqli = $this->connect();

            // sql を作る
            // エスケープをがんばろう
            $sql = "SELECT * FROM ". $table . " WHERE ". $id_name ." = " . $id . " AND " . $flag_name . " = 0";

            $list = Array();
            $res = new stdClass();
            if ($result = $mysqli->query($sql)) {
                $count_page = $result->num_rows;
                while ($row = $result->fetch_assoc()) {
                    array_push($list, $row);
                }
                $res->data = $list[0];
                $res->is_success = 1;
                $result->close();
            } else {
                $res->error = $mysqli->error;
            }
            if ( $res->error ) {
                return $res;
            }
            $close = $this->close($mysqli);
            return $res;
        }


        // select する処理
        public function select($params, $table, $page = 1, $limit = 20, $sort_order = "id", $sort_asc = "desc", $delimita = '*') {
            // connect する
            $mysqli = $this->connect();

            // sql を作る
            // エスケープをがんばろう
            $w = Array();
            foreach ( $params as $key => $val ) {
                array_push($w, $key ." = '". $val . "'");
            }
            $sql = "SELECT ".$delimita." FROM ". $table . " WHERE " . implode(" AND ", $w);

            // まずはcountから
            $total;
            if ($result = $mysqli->query($sql)) {
                $total = $result->num_rows;
                $result->close();
            } else {
                $res->error = $mysqli->error;
            }
            // エラーなら終わり
            if ( $res->error ) {
                return $res;
            }

            // pageを処理
            $ofs_limit = $this->ofs_limit($page, $limit, $total);

            // sort も処理
            $sort = $sort_order && $sort_asc ? $sort_order . " " . $sort_asc : "";

            $sql .= " ORDER by " . $sort . " " . $ofs_limit;
            $list = Array();
            $res = new stdClass;
            if ($result = $mysqli->query($sql)) {
                $count_page = $result->num_rows;
                while ($row = $result->fetch_assoc()) {
                    array_push($list, $row);
                }
                $res->list = $list;
                $res->is_success = 1;
                $result->close();
            } else {
                $res->error = $mysqli->error;
            }
            if ( $res->error ) {
                return $res;
            }
            $close = $this->close($mysqli);
            // pager を作成
            $res->pager = $this->pager($total, $limit, $page);

            return $res;
        }

        public function pager ( $total, $limit, $page = 1 ) {
            $pager = new stdClass;
            if ( $total && $limit ) {
                $pager->offset     = $offset = ( $page - 1 ) * $limit;
                $pager->page_total = ceil( $total / $limit );
                $pager->total      = $total;
                $pager->limit      = $limit;
                $pager->page       = $page;
                $pager->page_start = $page * $limit - $limit + 1;
                $pager->page_end   = ( $page == $pager->page_total ) ? ($total % $limit) + $page->total + $limit : ( $page * $limit ) ;
                $pager->next       = ( $page == $pager->page_total ) ? null : $page + 1;
                $pager->prev       = ( $page == 1 ) ? null : $page - 1;
                return $pager;
            }
        }

        // delete する処理
        // 中身は delete ではなく、delete_flag を立てる update
        public function delete( $table, $id ) {
            $mysqli = $this->connect();

            if ( ! $id ) {
                $res->error = "id is null";
                return $res;
            }

            // 最初は必ず connect
            $mysqli = $this->connect();

            // sql を作る
            // エスケープをがんばろう
            $columns = Array();

            $sql = "UPDATE ".$table." SET delete_flag = '1' WHERE id = " . $id;

            if( $mysqli->query( $sql ) ) {
                $res->is_success = 1;
            } else {
                $res->error = $mysqli->error;
            }
            $close = $this->close($mysqli);
            return $res;
        }
    }
?>
