<?php
    require_once(dirname(__FILE__)."/../lib/Core.php");
    // 苦肉の策 Skinny はここで読む
    require_once(dirname(__FILE__)."/../lib/Band/Skinny.php");

    class Contact {

        public function __construct() {
            $this->mlg = new Core();
            $this->path = Array(
                'form'     => "./",
                'confirm'  => "./confirm.php",
                'send'     => "./send.php",
                'finish'   => "./finished.php",
            );
            $this->page_id = "contact";
            $this->table_name = "contact";
            $this->page_name = "お問い合わせ";
            $this->sendmail_admin = CONTACT_TO;
            $this->sendmail_from = CONTACT_FROM;
            $this->sendmail_to = CONTACT_TO;
            $this->sendmail_bcc = CONTACT_BCC;
            $this->sendmail_title_admin = "お問い合わせがありました";
            $this->sendmail_title = "お問い合わせいただきありがとうございます";
            $this->template_dir = $this->mlg->config->project_root . "/template/eml/";
        }

        // controller
        // フォーム用
        public function form() {
            $res = new stdClass;
            $params = $this->mlg->tools->params();

            // controller
            if ( $this->mlg->tools->is_check($params, 'contact_form') ) {
                $params = $this->mlg->tools->get_session($params, 'contact_form');
                $check = $this->mlg->validate->do_check($params, $this->mlg->tools->getConfig("contact"));
                $res->has_error = $check["has_error"];
                $res->errors = $check["errors"];
            }
            $this->mlg->tools->delete_session('contact_form');
            $res->params = $params;
            return $res;
        }

        // 確認
        public function confirm() {
            $res = new stdClass;
            // params 取得
            $params = $this->mlg->tools->params();

            // まず CSRF
            try {
                $this->mlg->csrf->validate(filter_input(INPUT_POST, 'token'), true);
            } catch (\RuntimeException $e) {
                header('location: '.$this->path["form"]);
                exit();
            }

            // validateをかける
            $check = $this->mlg->validate->do_check($params, $this->mlg->tools->getConfig("contact"));
            // エラーがあればformにリダイレクト
            if ( $check["has_error"] ) {
                $errors = $check["errors"];
                // session に格納
                $params["is_check"] = 1;
                $this->mlg->tools->add_session($params, 'contact_form');
                $res->has_error = $check["has_error"];
                $res->errors = $check["errors"];
                header('location: '.$this->path["form"]);
                exit();
            }
            $res->params = $params;
            return $res;
        }

        public function send() {
            if ( $this->mlg->tools->params() ) {
                $params = $this->mlg->tools->params();

                // まず CSRF
                try {
                    $this->mlg->csrf->validate(filter_input(INPUT_POST, 'token'), true);
                } catch (\RuntimeException $e) {
                    header('location: '.$this->path["form"]);
                    exit();
                }

                // validateをかける
                $check = $this->mlg->validate->do_check($params, $this->mlg->tools->getConfig("contact"));
                // エラーがあればformにリダイレクト
                if ( $check["has_error"] ) {
                    // session に格納
                    $params["is_check"] = 1;
                    $this->mlg->tools->add_session($params, 'contact_form');
                    header('location: '.$this->path["form"]);
                    exit();
                }

                // mysql にインポートする場合
                // $res = $this->mlg->schema->insert($params, $this->table_name);

                // メールを送る場合
                // ユーザ向けメール
                $user_info = Array(
                    'from'  => $this->sendmail_from,
                    'to'    => $params["email"],
                    'header' => "From:".$this->sendmail_from . "\r\nReturn-Path: ".$this->sendmail_from,
                    'subject' => $this->sendmail_title,
                );
                // $this->mailset("contact_user.eml", $user_info, $params);
                // 本文を準備
                $body = $this->parse("contact_user.eml", $params);
                // 送信
                $this->mlg->sendmail->send($user_info["to"], $user_info["header"], $user_info["subject"], $body, $user_info["from"]);

                // 管理者向けメール
                $manager_info = Array(
                    'from'    => $this->sendmail_from,
                    'to'      => $this->sendmail_admin,
                    'header' => "From:".$this->sendmail_from . "\r\nBcc: ".$this->sendmail_bcc . "\r\nReturn-Path: ".$this->sendmail_from,
                    'subject' => $this->sendmail_title_admin,
                );
                // 本文を準備
                $mbody = $this->parse("contact_manager.eml", $params);
                // 送信
                $this->mlg->sendmail->send($manager_info["to"], $manager_info["header"], $manager_info["subject"], $mbody, $manager_info["from"]);

                // 完了したらfinish へ
                header('location: '.$this->path["finish"]);


            } else {
                // 値がなければ form へ
                header('location: '.$this->path["form"]);
            }
            return;
        }


        // skinnyでパースする
        // template のファイル名と、data を入れる
        // data は配列がいいとおもう
        public function parse($template, $data) {
            $sk = new Skinny();
            $filename = $this->template_dir . $template;
            // skinny でパース
            return $sk->SkinnyFetchHTML($filename, $data);
        }


    }

?>