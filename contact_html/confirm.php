<?php
$setPath= "../";
$pageTitle = "問い合わせ確認";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "スズキアリーナ茅ヶ崎の問い合わせ確認のページです。",
);
?>
<?php
session_start();
require_once(dirname(__FILE__)."/lib/Mulgu/controller/Common.php");
require_once(dirname(__FILE__)."/lib/Mulgu/controller/Contact.php");
$common = new Common();
$contact = new Contact();
$obj = $contact->confirm();
// params をなんやかんやする
$params = $obj->params;
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
</head>

<body class="contact">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">お問い合わせ</span><br><span class="en">CONTACT</span></h2>
    </div>
  </section>

  <section class="con01">
    <div class="wrapper">

      <form class="contact-form form -confirm" name="contact_form" id="contact-form" action="<?php echo $contact->path["send"]; ?>" method="POST">
          <?php echo $common->mlg->tools->params_to_hidden($params); ?>

        <table>

          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>お名前または会社名</span></th>
            <td class="contact-table-cont">
              <?php echo $params['name']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>メールアドレス</span></th>
            <td class="contact-table-cont">
              <?php echo $params['email']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl"><span>お問い合わせ種別</span></th>
            <td class="contact-table-cont">
              <?php echo $params['contact_type']; ?>
            </td>
          </tr>

          
          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>お問い合わせ内容</span></th>
            <td class="contact-table-cont">
              <?php echo nl2br($params['comment']); ?>
            </td>
          </tr>
        </table>


        <div class="contact-btn clearfix">
          <a href="javascript:void(0);" class="contact-btn-item -btn01 -confirm js-back">修正する</a>
          <a href="javascript:void(0)" class="contact-btn-item -btn01 -confirm js-send">この内容で送信する</a>
        </div>
      </form>
    </div>
  </section>


  <?php require_once($setPath.'lib/include/contact.php'); ?>
  <?php require_once($setPath.'lib/include/footer.php'); ?>

  <script>
    // 送信ボタンsubmit
    jQuery('.js-send').on('click', function() {
      do_submit(this,'contact_form');
    });

    // 修正ボタンsubmit
    jQuery('.js-back').on('click', function() {
      document.contact_form.action='<?php echo $contact->path["form"]; ?>';
      do_submit(this,'contact_form');
      return false;
    });

    // 問い合わせ
    function do_submit(a,fname) {
      // $(a).addClass('loading').text('... 送信中');
      document[fname].submit();
      return false;
    }
  </script>
  
</body>
</html>
