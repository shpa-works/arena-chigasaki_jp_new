;(function($) {

  jQuery.fn.ow_calender = function( $options ) {
    this.on('click', function(){

      // 今日の日付
      var $today = new Date();

      //年・月・日・曜日を取得する
      var $today_year = $today.getFullYear();
      var $today_month = $today.getMonth() + 1;

      // 初期値
      var $defaults = {
        year : $today_year,
        month : $today_month,
        input : ''
      };

      // 初期値とオプションをマージ
      var $params = $.extend($defaults, $options);

      create_calender( $params );
    });
  };

  function create_calender( $params ) {

    // 今日の日付
    var $today = new Date();

    //年・月・日・曜日を取得する
    var $today_day = $today.getDate();

    // 曜日
    var $week_arr = new Array( '日', '月', '火', '水', '木', '金', '土' );

    var $day_class;

    // その月の最初の日の情報
    var $date_start = new Date( $params.year, $params.month - 1 );
    // 最初の曜日取得
    var $start_week = $date_start.getDay();

    // その月の最後の日の情報
    var $date_end = new Date( $params.year, $params.month , 0 );
    // 最後の日取得
    var $end_day = $date_end.getDate();
    // 最後の曜日取得
    var $end_week = $date_end.getDay();

    // 前の年月
    if( $params.month <= 1 ) {
      $prev_year = $params.year - 1;
      $prev_month = 12;
    } else {
      $prev_year = $params.year;
      $prev_month = $params.month - 1;
    }
    $prev_date = $prev_year + '-' + $prev_month;

    // 次の年月
    if( $params.month >= 12 ) {
      $next_year = $params.year + 1;
      $next_month = 1;
    } else {
      $next_year = $params.year;
      $next_month = $params.month + 1;
    }
    $next_date = $next_year + '-' + $next_month;

    // 週折返し用カウンター
    var $week_count = 0;

    // カレンダーのhtmlここから
    var $calender_html = '<div class="ow-calender-wrapper">';

    $calender_html += '<div class="ow-calender-date">';
    $calender_html += '<div class="js-select-month -pager" data-date="' + $prev_date + '">&lt;</div>';

    // 選択年月
    $calender_html += '<span>' + $params.year + '/' + $params.month + '</span>';

    $calender_html += '<div  class="js-select-month -pager" data-date="' + $next_date + '">&gt;</div>';
    $calender_html += '</div>';

    // 曜日出力
    for ( var $i = 0; $i < $week_arr.length; $i++ ) {
      if( $i == 0 ){
        $calender_html += '<dl class="ow-calender-wrap">';
      }
      $calender_html += '<dd class="-col -week">' + $week_arr[$i] + '</dd>';
      if( $i == ( $week_arr.length - 1 ) ){
        $calender_html += '</dl>';
      }
    }

    // 最初の空の日付出力
    if( $start_week > 0 ) {
      for ( var $i = 0; $i < $start_week; $i++ ) {
        if( $i == 0 ){
          $calender_html += '<dl class="ow-calender-wrap">';
        }
        $calender_html += '<dd class="-col -empty"></dd>';
        $week_count++;
      }
    }

    // 日付出力
    for ( var $i = 1; $i <= $end_day; $i++ ) {
      var $day_class = '';
      if( ( $week_count % 7 ) == 0) {
        $calender_html += '</dl>';
        $calender_html += '<dl class="ow-calender-wrap">';
      }
      if( $i == $today_day ){
        $day_class = ' -today'
      }
      $calender_html += '<dd class="-col -day js-select-day' + $day_class + '" data-date="' + $params.year + '-' + $params.month + '-' + $i + '">' + $i + '</dd>';
      $week_count++;
    }
    // 残りの空の日付出力
    for ( var $i = 0; $i < ( 6 - $end_week ); $i++ ) {
      $calender_html += '<dd class="-col -empty"></dd>';
    }
    $calender_html += '</dl>';

    // 段数を合わせる
    if( $week_count < 36 ) {
      console.log('aa');
      $calender_html += '<dl class="ow-calender-wrap">';
      for ( var $i = 0; $i < 7; $i++ ) {
        $calender_html += '<dd class="-col -empty"></dd>';
      }
      $calender_html += '</dl>';
    }
    $calender_html += '</div>';
    // .js-calender の閉じ
    $calender_html += '</div>';
    // カレンダーのhtmlここまで

    // カレンダーがないなら、カレンダーを作ればいい
    if( jQuery('.ow-calender').length <= 0 ){
      var $overlay = '<div class="ow-overlay js-calender-close"></div>';
      var $calender = '<div class="ow-calender js-calender" data-input=""></div>';
      jQuery('body').prepend($overlay);
      jQuery('body').prepend($calender);
    }

    // カレンダー出力
    jQuery('.js-calender').attr( 'data-input', $params.input );
    jQuery('.js-calender').html($calender_html);
    jQuery('.js-calender').fadeIn();

  };

  // 月送り
  jQuery(document).on('click', '.js-select-month', function() {
    $str = jQuery(this).data('date');
    $select_date = $str.split('-');
    create_calender({
      year : Number($select_date[0]),
      month : Number($select_date[1])
    });
  });

  // inputに日付入力
  jQuery(document).on('click', '.js-select-day', function() {
    $select_date = jQuery(this).data('date');
    $select_input = jQuery('.js-calender').data('input');
    jQuery('.' + $select_input).val($select_date);
    jQuery('.js-calender').remove();
    jQuery('.ow-overlay').remove();
  });

  // カレンダー閉じる
  jQuery(document).on('click', '.js-calender-close', function() {
    jQuery('.js-calender').remove();
    jQuery('.ow-overlay').remove();
  });


})(jQuery);