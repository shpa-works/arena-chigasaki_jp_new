<?php
    ob_start();
    // gatasbal 本体
    class Core {
        public function __construct() {
            // まずはファイルをスキャン
            $class_dir = dirname(__FILE__)."/Class/";
            $files = scandir( $class_dir );

            foreach ( $files as $file ) {
                if ( preg_match("/^(.*?)\.php$/", $file, $match) ) {
                    $name = $match[1];

                    // 読み込む
                    require_once($class_dir . $name . ".php");
                    $lower = mb_strtolower($name);
                    $this->$lower = new $name();
                }
            }

            // 便利系、とりあえず
            $this->url_http = $this->config->url_http;
            $this->rel_base = $this->config->rel_base;

        }
    }
?>