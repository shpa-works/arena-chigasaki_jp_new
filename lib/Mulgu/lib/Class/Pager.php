<?php
/*
 * (c) copyright Hiroya Fujii
 * http://blog.3ot.net/design/php/20090915185455.html
 * license http://opensource.org/licenses/mit-license.php MIT License
 *
 * custmized by mikadukimo
 */

class Pager {

// 番号リンクの数
  public $linksNum = 6;
// 1ページに表示するデータ数
  public $parOnePage = 20;
// 「いちばん最初リンク」と「いちばん最後リンク」のところの数。
  public $topEnd = 2;
// 「いちばん最初リンク」と「いちばん最後リンク」の区切り文字。
  public $between = '...';
// prev, nextの文字列
  public $prevString = 'PREV';
  public $nextString = 'NEXT';
// prev, クエリ文字列
  public $query = 'page';
// ulタグにつくクラス名
  public $class = 'pager';

  function __construct($options=false){
    if(is_array($options) && count($options) > 0){
      array_walk($options, array($this, 'options'));
    }
  }
  function setting($options=false) {
    if(is_array($options) && count($options) > 0){
      array_walk($options, array($this, 'options'));
    }
  }

  function doIt($url, $total, $query=array()){
    if(!$total){ return false; }
    $current = isset($_GET[$this->query]) ? $_GET[$this->query] : 1;
    $pages = ceil($total / $this->parOnePage);
    $prev = $current > 1 ? $current - 1 : false;
    $next = $current < $pages ? $current + 1 : false;
    $left = $current - ceil($this->linksNum/2);
    $right = $current + ceil($this->linksNum/2);
    if($left < 1){
      while($right <= $this->linksNum){
        $right++;
      }
      $left = 1;
    }
    if($right > $pages){
      $left = $left - $right + $pages;
      $left = $left < 1 ? 1 : $left;
      $right = $pages;
    }
    for($i = $left; $i <= $right; $i++){
      $temp[] = $i;
    }
    if($temp[0] > 1){
      for($i = 1; $i < $temp[0] && $i <= $this->topEnd; $i++){
        $top[] = $i;
      }
    }
    $top = isset($top) ? $top : array();
    if(count($top) > 0 && $top[count($top) - 1] != $temp[0] - 1){
      array_push($top, $this->between);
    }
    $last = $temp[count($temp) - 1];
    if($last < $pages - $this->topEnd){
      array_push($temp, $this->between);
    }
    for($i = 0; $i < $this->topEnd; $i++, $pages--){
      if($pages > $last){
        $bottom[] = $pages;
      }
    }
    $bottom = isset($bottom) ? array_reverse($bottom) : array();
    $temp = array_merge($top, $temp, $bottom);
    if($prev){
      $pager[] = '<li class="pager-item pager-item-prev"><a href="'.$url.$this->pagerQuery($prev, $query).'" class="pager-btn">'.$this->prevString.'</a></li>'."\n";
    } else {
      $pager[] = '<li class="pager-item pager-item-prev"><span class="pager-btn">'.$this->prevString.'</span></li>'."\n";
    }
    for($i = 0; $i < count($temp); $i++){
      if($temp[$i] == $this->between){
        $str = $this->between;
      }elseif($current == $temp[$i]){
        $str = '<span class="pager-btn">'.$temp[$i].'</span>';
      }else{
        $str = '<a href="'.$url.$this->pagerQuery($temp[$i], $query).'" class="pager-btn">'.$temp[$i].'</a>';
      }
      $pager[] = '<li class="pager-item">'.$str.'</li>'."\n";
    }
    if($next){
      $pager[] = '<li class="pager-item pager-item-next"><a href="'.$url.$this->pagerQuery($next, $query).'" class="pager-btn">'.$this->nextString.'</a></li>'."\n";
    } else {
      $pager[] = '<li class="pager-item pager-item-next"><span class="pager-btn">'.$this->nextString.'</span></li>'."\n";
    }
    return '<ol class="pager">'.implode($pager).'</ol>';
  }

  function options($v, $k){
    $this->$k = $v;
  }

  function pagerQuery($page, $query){
    if(isset($_GET[$this->query])){
      unset($_GET[$this->query]);
    }
    $query = isset($_GET) ? array_merge($_GET, $query) : $query;
    if(count($query) > 0){
      foreach($query as $k => $v){
        $temp[] = $k.'='.$v;
      }
    }
    if($page > 1){
      $temp[] = $this->query.'='.$page;
    }
    return isset($temp) ? '?'.implode('&', $temp) : '';
  }

}