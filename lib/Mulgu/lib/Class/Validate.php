<?php
    require_once(dirname(__FILE__)."/../Class/Schema.php");

/*
○使えるデータ型一覧
    text_half   ... 半角英数のみ
    passwd      ... 8～100 の英数記号のみ
    email       ... ある程度正しいメールアドレスの形の文字列のみ
    tel         ... ある程度正しい電話番号の形の文字列のみ - はどちらでOK
    zip         ... 郵便番号の形の文字列のみ - はどちらでOK
    integer     ... 数字のみ
    date        ... yyyy-mm-dd
*/

    class Validate {
        // form に入力された値のvalidate
        // 足りない型はここにcaseで追加
        public function do_check($params = Array(), $config = Array(), $skip = Array() ) {
            $column = $config["column"];
            $table_name = $config["table_name"];

            // エラーが無いか確認する
            $has_error = 0;

            $errors = Array();
            foreach ($column as $key => $value) {
                foreach ( $skip as $sk => $sv ) {
                    if ( $key == $sv ) {
                        $is_skip = 1;
                    }
                }
                if ( $is_skip ) {
                    continue;
                }
                // 必須かどうか
                if ( $value["must"] ) {
                    // 必須ならデータが入ってるか確認
                    if ( empty($params[$key]) ) {
                        $has_error = 1;
                        $errors[$key] = "「".$column[$key]["name"]."」は必須項目です";
                    }
                }
                if ( $value["unique"] && $config["use_db"] == true ) {
                    // unique なら他に同じデータがないか調べる
                    $schema = new Schema();
                    $res = $schema->select(Array($key => $params[$key]), $table_name);
                    $list = Array();
                    // 自分と同じデータか調べる
                    foreach ( $res->list as $uv ) {
                        if ( $uv["id"] != $params["id"] ) {
                            array_push($list, $uv);
                        }
                    }
                    $exists = count($list);
                    if ( ! empty($params[$key]) && $exists > 0 ) {
                        $has_error = 1;
                        $errors[$key] = "「".$params[$key]."」はすでに使用されています";
                    }
                }
                // case文で各データ型を照合
                switch ($column[$key]["type"]) {
                    case "text_half":
                        if( $params[$key] && ! preg_match('/^[!-~]+$/', $params[$key])){
                            $has_error = 1;
                            $errors[$key] = "「".$column[$key]["name"]."」には半角英数のみ入力可能です";
                        }
                        break;
                    case "passwd":
                        if( $params[$key] && ! preg_match('/\A[a-z\d]{8,100}+\z/i', $params[$key])){
                            $has_error = 1;
                            $errors[$key] = "「".$column[$key]["name"]."」には8文字以上の半角英数と記号のみを入力してください";
                        }
                        break;
                    case "email":
                        if( $params[$key] && ! preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/iD', $params[$key])){
                            $has_error = 1;
                            $errors[$key] = "「".$column[$key]["name"]."」が正しく入力されていません";
                        }
                        if ( $column[$key]["double"] == 1 ) {
                            if ( $params[$key] != $params[$key."_double"] ) {
                                $has_error = 1;
                                $errors[$key."_double"] = "「".$column[$key]["name"]."」が一致しません";
                            }
                        }
                        break;
                    case "tel":
                        if( $params[$key] && ! preg_match('/^\d{2,5}\-?\d{2,4}\-?\d{4}$/', $params[$key])){
                            $has_error = 1;
                            $errors[$key] = "「".$column[$key]["name"]."」が正しく入力されていません";
                        }
                        break;
                    case "zip":
                        if( $params[$key] && ! preg_match('/^\d{3}\-?\d{4}$/', $params[$key])){
                            $has_error = 1;
                            $errors[$key] = "「".$column[$key]["name"]."」が正しく入力されていません";
                        }
                        break;
                    case "integer":
                        if( $params[$key] && ! preg_match('/^\d+$/', $params[$key])){
                            $has_error = 1;
                            $errors[$key] = "「".$column[$key]["name"]."」が正しく入力されていません";
                        }
                        break;
                    case "date":
                        if( $params[$key] && ! preg_match('/^\d{4}\-\d{1,2}\-\d{1,2}$/', $params[$key])){
                            $has_error = 1;
                            $errors[$key] = "「".$column[$key]["name"]."」が正しく入力されていません";
                        }
                        break;
                    case "agree":
                        if( ! $params[$key] ) {
                            $has_error = 1;
                            $errors[$key] = $column[$key]["name"]."に同意してください";
                        }
                        break;
                }
            }
            if ( $has_error ) {
                return ( Array( has_error => $has_error, errors => $errors ) );
            } else {
                return;
            }
        }
    }
?>