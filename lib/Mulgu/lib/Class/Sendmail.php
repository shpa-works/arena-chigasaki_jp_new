<?php
    require_once(dirname(__FILE__)."/../Band/Skinny.php");

    class Sendmail {
        public function __construct() {
            // skinny をロード
            $this->template_dir = dirname(__FILE__) . "/../../template/eml/";
        }

        // メールの準備と送信
        public function mailset( $template, $info, $params ) {
            // 本文を準備
            $body = $this->parse($template, $params);

            // 送信
            $this->send($info["to"], $info["header"], $info["subject"], $body, $info["from"]);
        }

        // skinnyでパースする
        // template のファイル名と、data を入れる
        // data は配列がいいとおもう
        public function parse($template, $data) {
            $sk = new Skinny();
            $filename = $this->template_dir . $template;
            // skinny でパース
            return $sk->SkinnyFetchHTML($filename, $data);
        }

        // メールを送る部分
        // Mail::Mime とかにしたい
        public function send($to, $header, $subject, $body, $from) {
            mb_language("Japanese");
            mb_internal_encoding("UTF-8");

            // 送信
            if( mb_send_mail($to, $subject, $body, $header, '-f' . $from) ){
                return 1;
            } else {
                return;
            }
        }

    }

?>