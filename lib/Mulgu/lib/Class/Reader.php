<?php
    require_once(dirname(__FILE__)."/Config.php");

    class Reader {
        public function get_xml($url, $limit = 10) {
            $rss = file_get_contents($url);
            $rss = preg_replace("/<([^>]+?):(.+?)>/", "<$1_$2>", $rss);
            $rss = simplexml_load_string($rss,'SimpleXMLElement',LIBXML_NOCDATA);
            $res = Array();
            foreach ( $rss->channel->item as $c ) {
                if ( count($res) < $limit ) {
                    $c->thumbnail = $c->media_thumbnail->attributes()->url;
                    array_push($res, $c);
                } else {
                    break;
                }
            }
            return $res;
        }
    }

?>