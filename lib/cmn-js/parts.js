/*=============================================
　　PAGE TOP
==============================================*/
$(function () {
    var topBtn = $('#page-top');
    topBtn.hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });

    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});


/*=============================================
　　SCROLL
==============================================*/
var headerHeight = $('header').outerHeight();
var urlHash = location.hash;
if(urlHash) {
    $('body,html').stop().scrollTop(0);
    setTimeout(function(){
        var target = $(urlHash);
        var position = target.offset().top - headerHeight;
        $('body,html').stop().animate({scrollTop:position}, 500);
    }, 100);
}
$('a[href^="#"]').click(function() {
    var href= $(this).attr("href");
    var target = $(href);
    var position = target.offset().top - headerHeight;
    $('body,html').stop().animate({scrollTop:position}, 500);   
});


/*=============================================
　　VIEWPORT
==============================================*/
$(function () {
    var ua = {
        iPhone: navigator.userAgent.indexOf('iPhone') != -1,
        iPad: navigator.userAgent.indexOf('iPad') != -1,
        iPod: navigator.userAgent.indexOf('iPod') != -1,
        android: navigator.userAgent.indexOf('Android') != -1,
        windows: navigator.userAgent.indexOf('Windows Phone') != -1
    }
    if (ua.iPhone || ua.iPod || ua.android || ua.windows) {
    } else {
    }
}());


/*=============================================
　　MENU
==============================================*/
/*SP MENU*/
$(function () {
  var $body = $('body');
  $('#navbtn').on('click', function () {
      $body.toggleClass('open');
  });
  $('.gnav a').on('click', function () {
      $body.removeClass('open');
  });
});



/*=============================================
　　MATCH HEIGHT
==============================================*/
$(function () {
    $('.height').matchHeight({property: 'min-height'});
});


/*=============================================
　　HEADER SHADOW
==============================================*/
$(window).scroll(function(){
  var element = $('header'),
       scroll = $(window).scrollTop(),
       height = element.outerHeight();
  if ( scroll > height ) {
    element.addClass('shadow');
  } else {
    element.removeClass('shadow');
  }
});

/*=============================================
　　MV SLIDER
==============================================*/
$('.slider').slick({
  autoplay:true,
  autoplaySpeed:3000,
  speed: 1000,
  dots:true,
  fade:true,
  dotsClass: 'slide-dots',
  arrows: false,
});

/*=============================================
　　TAB SWITCH
==============================================*/
document.addEventListener('DOMContentLoaded', function(){
  const tabs = document.getElementsByClassName('tab');
  for(let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', tabSwitch);
  }
  function tabSwitch(){
    document.getElementsByClassName('is-active')[0].classList.remove('is-active');
    this.classList.add('is-active');
    document.getElementsByClassName('is-show_panel')[0].classList.remove('is-show_panel');
    const arrayTabs = Array.prototype.slice.call(tabs);
    const index = arrayTabs.indexOf(this);
    document.getElementsByClassName('panel')[index].classList.add('is-show_panel');
  };
});


/*=============================================
　　PARTS ANIMATION
==============================================*/
$(function(){
  $(".inview").on("inview", function (event, isInView) {
    if (isInView) {
      $(this).stop().addClass("is-show");
    }
  });
});