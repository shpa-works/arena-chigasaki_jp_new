<?php
if (empty($pageInfo["title"])) {
  $title = "スズキアリーナ茅ヶ崎は試乗だけでも大歓迎";
} else {
  $title = $pageInfo["title"] . " | スズキアリーナ茅ヶ崎は試乗だけでも大歓迎";
}

if (empty($pageInfo["keywords"])) {
  $keywords = "スズキアリーナ茅ヶ崎株式会社,未使用車,試乗,代理店,コバック";
} else {
  $keywords = $pageInfo["keywords"];
}

if (empty($pageInfo["description"])) {
  $description = "スズキアリーナ茅ヶ崎は、湘南エリアでスズキの新車・中古車をお探しの方に向けて販売店オプション装備済みの試乗車を豊富にご用意しています。車の購入や点検整備・自動車保険のご相談はぜひ弊社にお任せください。まずは簡単試乗予約へ。";
} else {
  $description = $pageInfo["description"];
}
?>

<?php
//echo $_SERVER['REQUEST_URI'];;
//https://test.arena-chigasaki.jp/
?>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/google-analytics.php'); ?>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title;?></title>
<meta name="format-detection" content="telephone=no,email=no">
<meta name="keywords" content="<?php echo $keywords; ?>">
<meta name="description" content="<?php echo $description; ?>">
<meta name="facebook-domain-verification" content="hrb92ymzfxeuvjcouame2lj7tvdjji" />
<link rel="icon" href="/lib/favicon/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="apple-touch-icon" sizes="180x180" href="lib/favicon/apple-touch-icon.png">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="/lib/cmn-css/common.css" media="all">
<link rel="stylesheet" href="/lib/cmn-css/slick/slick-theme.css" media="all">
<link rel="stylesheet" href="/lib/cmn-css/slick/slick.css" media="all">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>