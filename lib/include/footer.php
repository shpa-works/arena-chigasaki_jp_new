<section class="shop_info">
  <div class="wrapper">
    <h2>SHOP INFORMATION</h2>
    <div class="img_txt clearfix">
      <div class="imgbox"><img src="/lib/cmn-img/index/shop@2x.jpg" alt="スズキアリーナ茅ヶ崎株式会社"></div>
      <div class="txtbox">
        <p class="tit">スズキアリーナ茅ヶ崎株式会社</p>
        <p class="txt01">〒253-0041　神奈川県茅ヶ崎市茅ヶ崎2-1-50<br>TEL <a href="tel:0467-87-1883">0467-87-1883 </a>　　<br class="sp-on">FAX 0467-87-1884		</p>
        <div class="info">
          <p class="mintit"><span>お車でお越しのお客様</span></p>
          <p class="txt">平塚方面からお越しの方は、国道１号線を横浜方面へ走り「茅ケ崎駅前交差点」を過ぎた左手になります。茅ヶ崎中央病院さまの隣です。<br>藤沢方面からお越しの方は、国道１号線を小田原方面へ走り「一里塚交差点」を過ぎた右手になります。右折専用レーンの途中になりますので、ご来店の際はご注意くださいませ。</p>
          <p class="mintit"><span>公共交通機関をご利用のお客様</span></p>
          <p class="txt">電車をご利用の方は、JR茅ケ崎駅北口より徒歩5分です。</p>
        </div>
      </div>
    </div>

    <div class="recruit_bnr">
      <a href="https://saiyo.page/485739/job-list/sort=osusume&page=1" target="_blank">
        <img class="pc-on" src="/lib/cmn-img/index/recruit_bnr_pc@2x.png" alt="採用情報">
        <img class="sp-on" src="/lib/cmn-img/index/sp/recruit_bnr_sp@2x.png" alt="採用情報">
      </a>
    </div>
  </div>

  <div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3254.893587549171!2d139.40404501549077!3d35.333463880276724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x601852620b8e333b%3A0x712a0bee291c27cf!2z44CSMjUzLTAwNDEg56We5aWI5bed55yM6IyF44O25bSO5biC6IyF44O25bSO77yS5LiB55uu77yR4oiS77yV77yQ!5e0!3m2!1sja!2sjp!4v1630895571433!5m2!1sja!2sjp" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
  </div>
</section>
<footer>
  <div class="wrapper">
    <div class="logo_nav clearfix">
      <div class="logo">
        <a href="/">
          <ul>
            <li class="logo_img"><img src="/lib/cmn-img/common/logo.svg" alt="スズキアリーナ茅ヶ崎店"></li>
            <li class="logo_txt">スズキアリーナ茅ヶ崎店</li>
          </ul>
        </a>
      </div>

      <ul class="nav clearfix">
        <li><a href="/news/">新着情報</a></li>
        <li><a href="/company">会社概要</a></li>
        <li><a href="/contact">お問い合わせ</a></li>
        <li><a href="/#testdrive">試乗車予約</a></li>
        <li><a href="/privacypolicy">プライバシーポリシー</a></li>
      </ul>
    </div>
    <p class="copyright">©︎ Arena-chigasaki All Rights Reserved.</p>
  </div>
</footer>

<!--<div id="page-top"><a href="#"><img src="lib/cmn-img/common/pagetop.jpg"></a></div>-->

<script src="/lib/cmn-js/all.js"></script>
<script src="/lib/cmn-js/parts.js"></script>