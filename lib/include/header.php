<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PXQ8WMK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
  <div class="wrapper clearfix">
    <div class="logo">
      <a href="/">
        <ul>
          <li class="logo_img"><img src="/lib/cmn-img/common/logo.svg" alt="スズキアリーナ茅ヶ崎店"></li>
          <li class="logo_txt"><h1>スズキアリーナ茅ヶ崎店</h1></li>
        </ul>
      </a>
    </div>
    <nav class="gnav">
      <ul>
        <li class="news"><a href="/news/"><span>新着情報</span></a></li>
        <li class="testdrive"><a href="/#testdrive"><span>試乗車予約</span></a></li>
        <li class="recruit"><a href="https://saiyo.page/485739/job-list/sort=osusume&page=1" target="_blank"><span>求人情報</span></a></li>
        <li class="company"><a href="/company"><span>会社概要</span></a></li>
        <li class="contact"><a href="/contact"><span>お問い合わせ</span></a></li>
      </ul>

      <div class="box tel height sp-on">
        <p class="tit">お電話でお問い合わせ</p>
        <p class="number"><span><a href="tel:0467-87-1883">0467-87-1883</a></span></p>
        <p class="txt">営業時間：9:00〜18:00　定休日：水曜日</p>
      </div>
    </nav>
    <button type="button" id="navbtn" class="navbtn"></button>
  </div>
</header>