<section class="contactArea">
  <div class="wrapper">
    <h2 class="tit01">
      <span class="jp">お問い合わせ</span><br>
      <span class="en">CONTACT</span>
    </h2>
    <div class="boxwrap clearfix">
      <div class="box tel height">
        <p class="tit">お電話でお問い合わせ</p>
        <p class="number"><span><a href="tel:0467-87-1883">0467-87-1883</a></span></p>
        <p class="txt">営業時間：9:00〜18:00　定休日：水曜日</p>
      </div>
      <div class="box mail height">
        <p class="tit">メールでお問い合わせ</p>
        <a href="/contact"><span>お問い合わせフォーム</span></a>
      </div>
    </div>
  </dvi>
</section>