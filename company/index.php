<?php
$setPath= "../";
$pageTitle = "会社概要";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "スズキアリーナ茅ヶ崎の会社概要のご案内です。",
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
</head>

<body class="company">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">会社概要</span><br><span class="en">COMPANY</span></h2>
    </div>
  </section>

  <section class="con01">
    <div class="wrapper">
      <p class="message">
        皆様、日頃より当グループをご愛顧いただき誠に感謝申し上げます。<br>
        昭和21年に茅ヶ崎市で創業し、これまでずっと地元の皆様に支えられ続け、<br class="pc-on">私で4代目の代表となりました。<br>ここまで事業を続けてこれていることに深く感謝申し上げます。<br>
        そんな我々が出来ること、やらなければならないことは、地域の皆様のお役に立てるよう地域貢献に努め、茅ヶ崎から湘南地域を元気にしていくことだと思っております。
      </p>

      <div class="content">
        <div class="company_info policy">
          <p class="title">経営理念</p>
          <p class="head">「集った仲間と共に、<br class="sp-on">必要とされ続ける組織となる」</p>
        </div>
      </div>

      <div class="content">
        <div class="company_info">
          <table>
            <tr>
              <th>会社名</th>
              <td>スズキアリーナ茅ヶ崎株式会社</td>
            </tr>
            <tr>
              <th>所在地</th>
              <td>〒253-0041 神奈川県茅ヶ崎市茅ヶ崎2-1-50</td>
            </tr>
            <tr>
              <th>電話番号</th>
              <td>0467-87-1883</td>
            </tr>
            <tr>
              <th>FAX番号</th>
              <td>0467-87-1884</td>
            </tr>
            <tr>
              <th>設立</th>
              <td>昭和51年3月4日</td>
            </tr>
            <tr>
              <th>代表者名</th>
              <td>代表取締役　山口　裕太</td>
            </tr>
            <tr>
              <th>古物商許可証番号</th>
              <td>第452675200421</td>
            </tr>
            <tr>
              <th>主な取引先</th>
              <td>スズキ株式会社・株式会社スズキ自販湘南・損害保険ジャパン株式会社・三井住友海上火災保険株式会社・あいおいニッセイ同和損害保険株式会社・株式会社ジャックス・株式会社オリエントコーポレーション・住友三井オートサービス株式会社・株式会社コバック・一般社団法人日本福祉車両協会</td>
            </tr>
            <tr>
              <th>資本金</th>
              <td>1,000万円</td>
            </tr>
            <tr>
              <th>事業内容</th>
              <td>・スズキ車の新車販売<br>・中古車販売<br>・自動車の整備、点検修理<br>（関東運輸局指定整備事業場・関東運輸局認証整備事業場）<br>・自動車関連部品用品販売 <br>・保険代理業（損害保険・生命保険）<br>・JAF入会受付<br>・福祉車両の整備、点検修理<br>（一般社団法人日本福祉車輌協会認定福祉車輌取扱士）</td>
            </tr>
          </table>
        </div>
      </div>

      <div class="content">
        <h3 class="tit01">沿革</h3>
        <div class="history">
          <table>
            <tr>
              <th>1946年（昭和21年）</th>
              <td>金子自動車工業所　設立<br>自動車の修理・販売</td>
            </tr>
            <tr>
              <th>1976年（昭和51年）</th>
              <td>金子自動車株式会社に社名変更</td>
            </tr>
            <tr>
              <th>1979年（昭和54年）</th>
              <td>ジェミニオート茅ヶ崎株式会社　設立<br>いすゞ取扱ディーラーとして営業</td>
            </tr>
            <tr>
              <th>1991年（平成3年）</th>
              <td>金子自動車株式会社をスズキカルタス茅ケ崎株式会社に社名変更<br>スズキ小型正規ディーラーとして営業</td>
            </tr>
            <tr>
              <th>1997年（平成9年）</th>
              <td>ジェミニオート茅ヶ崎株式会社の自動車部門をスズキカルタス茅ケ崎株式会社に統合</td>
            </tr>
            <tr>
              <th>2000年（平成12年）</th>
              <td>スズキカルタス茅ケ崎株式会社をスズキアリーナ茅ヶ崎株式会社へ社名変更</td>
            </tr>
            <tr>
              <th>2009年（平成21年）</th>
              <td>ジェミニオート茅ヶ崎株式会社をジェミニ株式会社へ社名変更</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </section>
  
  <?php require_once($setPath.'lib/include/contact.php'); ?>
  <?php require_once($setPath.'lib/include/footer.php'); ?>
</body>
</html>
