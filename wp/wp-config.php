<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * データベース設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** データベース設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'kschigasaki_wp3' );

/** データベースのユーザー名 */
define( 'DB_USER', 'kschigasaki_wp3' );

/** データベースのパスワード */
define( 'DB_PASSWORD', 'tu5thwi9nl' );

/** データベースのホスト名 */
define( 'DB_HOST', 'localhost' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'uehe|6Zss>8HA|QhsfVZ}^;)kXh^f%GZGB!)sxxOZut~<%*ElMdEGw3QXlW{^f 9' );
define( 'SECURE_AUTH_KEY',  'Ei5Hs+gW<?U>$ ord<iH$sIo~-E#D.Qsh}F>c/3lpT@nanvW PH::;G&!22[oQ R' );
define( 'LOGGED_IN_KEY',    'd :g3Tw~m{;>t|Yls}d&Bl,|@0/#@!Qxz7y9tj>ZMblWCX&i7PO]xT9Q~D5C](y0' );
define( 'NONCE_KEY',        '#1`l.a~F]S?%r)!y9?plH=3whL.1eZr2Kg jgNInye1^N0_f;0_!~lEa-f9^[oc-' );
define( 'AUTH_SALT',        '<7KVlGfdoNLN^vg/hBZA19c|6rn-<8D 4lHF#4/r_zm}kG07k)V+!C=KVI? 2Y|9' );
define( 'SECURE_AUTH_SALT', 'Ny#:G,`K<UG1VE/b,Iew<PjU8GqPRQioR[M3jVV]at`S$1220W#jFS&.-qkW25N_' );
define( 'LOGGED_IN_SALT',   '-9Qu=v;zl.N7VU9LC,y]L64KJ-;+sqRD1zk+0/Kj]YUb%Y`?on{xTBxR$+EV7(s5' );
define( 'NONCE_SALT',       'tZ#oyHk)_:{3#>lP#-F[H2|wAiZqJ~<ZMSH)L,r!wX>O1;Y;GE[%}F~<S@z<2Q5Z' );
define( 'WP_CACHE_KEY_SALT','S4hJB26J6HH@#^m0Lm|ntar`elH[~6Yg_IVRF:D1pZ)o`C{st/-lzmy]45UHQb(?' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* カスタム値は、この行と「編集が必要なのはここまでです」の行の間に追加してください。 */



/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
