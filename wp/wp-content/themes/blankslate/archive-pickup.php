<?php
$setPath= "./";
$pageTitle = "厳選中古車";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
  <link rel="stylesheet" href="/lib/pickup/css/style.css" media="all">
</head>

<body id="pickup">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">厳選中古車</span><br><span class="en">PICK UP</span></h2>
    </div>
  </section>

  <section class="conts">
    <div class="wrap">
      <div class="carlist">
        <ul>

          <?php
          // ページネーションに現在のページ位置を知らせるのに必要
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

          $args = array(
            'posts_per_page' => 12, // 記事一覧で表示するページ数
            'paged' => $paged,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'pickup',
            'post_status' => 'publish'
          );

          // 記事一覧のMaxページ数を取得するのに必要
          $the_query = new WP_Query($args);

          // 記事一覧のループスタート
          if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post();

            $f_grade = get_field('f_grade'); // グレード
            $f_price_body = get_field('f_price_body'); // 車両本体価格(税込)
            $f_price_pay = get_field('f_price_pay'); // 支払総額(税込)
            $f_year = get_field('f_year'); // 年式
            $f_number = get_field('f_number'); // 車台番号
            $f_safety = get_field('f_safety'); // 車検有無
            $f_range = get_field('f_range'); // 走行距離
            $f_color = get_field('f_color'); // ボディカラー
            $f_option = get_field('f_option'); // オプション
            $f_photo_main = get_field('f_photo_main'); // 画像（メイン）
            $f_check = get_field('f_check'); // 整備
            $f_repair = get_field('f_repair'); // 修復歴
            $f_security = get_field('f_security'); // 保証
            $f_notes = get_field('f_notes'); // 補足
          ?>

          <li>
            <a href="<?php the_permalink(); ?>">
              <div class="carlist-inner">
                <div class="carlist-photo"><img src="<?php echo $f_photo_main ?>" alt="<?php the_title(); ?>"></div>
                <div class="carlist-title">
                  <p class="carlist-title-name"><?php the_title(); ?></p>
                  <p class="carlist-title-grade"><?php echo $f_grade; ?></p>
                </div>
                <dl class="carlist-price">
                  <dt>支払総額(税込)</dt>
                  <dd class="red"><span><?php echo $f_price_pay; ?></span>万円</dd>
                </dl>
                <div class="carlist-notes">
                  <dl>
                    <dt>車両価格</dt>
                    <dd><?php echo $f_price_body; ?>万円（税込）</dd>
                    <dt>諸費用</dt>
                    <dd>
                      <?php
                        // 支払総額 - 車両価格 = 諸費用
                        if ($f_price_pay != '' && $f_price_body != '') {
                          $overhead = floatval($f_price_pay) - floatval($f_price_body);
                          echo $overhead . '万円（税込）';
                        }
                      ?>
                    </dd>
                  </dl>
                  <p class="security">
                    <?php
                      $out_check = '';
                      if ($f_check == '定期点検整備つき') {
                        $out_check = 'あり';
                      } else {
                        $out_check = 'なし';
                      }

                      $out_security = '';
                      if ($f_security == '保証付') {
                        $out_security = '（' . $f_notes . '）';
                      }

                      $out_repair = '';
                      if ($f_repair == '修復歴あり') {
                        $out_repair = '／修復歴有り';
                      } else {
                        $out_repair = '／修復歴無し';
                      }
                      echo '法定整備' . $out_check . '／' . $f_security . $out_security . $out_repair;
                    ?>
                  </p>
                </div>
                <dl class="carlist-details">
                  <dt>年式</dt>
                  <dd><?php echo $f_year; ?></dd>
                  <dt>車台番号</dt>
                  <dd><?php echo $f_number; ?></dd>
                  <dt>車検有無</dt>
                  <dd><?php echo $f_safety; ?></dd>
                  <dt>走行距離</dt>
                  <dd><?php echo $f_range; ?></dd>
                  <dt>ボディカラー</dt>
                  <dd><?php echo $f_color; ?></dd>
                </dl>
                <p class="carlist-link"><span>詳しくみる</span></p>
              </div>
            </a>
          </li>

          <?php
            endwhile;
          endif;

          // 記事一覧のループ終わり
          wp_reset_postdata();
          ?>

        </ul>
      </div>

      <div class="pagination">
        <?php the_posts_pagination(
          array(
            'mid_size'      => 2, // 現在ページの左右に表示するページ番号の数
            'prev_next'     => true, // 「前へ」「次へ」のリンクを表示する場合はtrue
            'prev_text'     => __( '＜'), // 「前へ」リンクのテキスト
            'next_text'     => __( '＞'), // 「次へ」リンクのテキスト
            'type'          => 'list', // 戻り値の指定 (plain/list)
          )
        ); ?>
      </div>
    </div>
  </section>

  <section class="contactArea">
    <div class="wrapper">
      <h2 class="tit01">
        <span class="jp">お問い合わせ</span><br>
        <span class="en">CONTACT</span>
      </h2>
      <div class="boxwrap clearfix">
        <div class="box tel height">
          <p class="tit">お電話でお問い合わせ</p>
          <p class="number"><span><a href="tel:0467-87-1883">0467-87-1883</a></span></p>
          <p class="txt">営業時間：9:00〜18:00　定休日：水曜日</p>
        </div>
        <div class="box mail height">
          <p class="tit">メールでお問い合わせ</p>
          <a href="/contact"><span>お問い合わせフォーム</span></a>
        </div>
      </div>
    </dvi>
  </section>

  <?php require_once($setPath.'lib/include/footer.php'); ?>
  <?php wp_footer(); ?>
</body>
</html>