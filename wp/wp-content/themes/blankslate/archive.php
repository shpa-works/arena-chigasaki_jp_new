<?php
$setPath= "../";
$pageTitle = "新着情報一覧";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "スズキアリーナ茅ヶ崎の新着情報一覧のご案内です。",
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/head.php'); ?>
</head>

<body class="news">
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">ブログ</span><br><span class="en">BLOG</span></h2>
    </div>
  </section>

  <section class="con01">
    <div class="wrap">
      <div class="news-title"><span>記事一覧</span></div>

      <div class="categoryArea">
        <div class="category-title">カテゴリー</div>
        <ul class="categoryList">

        <?php
          $categories = get_categories();

          // 説明フィールドの並び順にソート
          $arySort = [];
          foreach ( $categories as $key => $value ) {
            $arySort[] = $value->category_description;
          }
          array_multisort($arySort, SORT_ASC, $categories);

          foreach ( $categories as $category ) {
            $cat_name = $category->name;
            $cat_link = $category->slug;
            $cat_sort = $category->category_description;

            // 説明フィールドが入力済みの場合のみ表示
            if ($cat_sort != "") {
              echo '<li>';
              echo   '<a href="/news/' . $cat_link . '/" class="' . $cat_link . '">';
              echo     $cat_name;
              echo   '</a>';
              echo '</li>';
            }
          }
        ?>

        </ul>
      </div>

      <div class="newsArea">
        <ul>
        <?php
          foreach ( $posts as $post ):
            $category = get_the_category(); 
            $cat_name = $category[0]->name;
            $cat_slug = $category[0]->slug;
        ?>

          <li>
            <a href="<?php the_permalink(); ?>">
              <div class="photoArea">
                <p class="photo">
                <?php if (has_post_thumbnail()): ?>
                  <?php the_post_thumbnail(); ?>
                <?php else: ?>
                  <img src="/lib/cmn-img/news/dummy.jpg" alt="">
                <?php endif; ?>
                </p>
                <p class="category <?php echo $cat_slug; ?>"><?php echo $cat_name; ?></p>
              </div>
              <div class="detailArea">
                <p class="date"><?php echo get_the_date( 'Y.m.d' ); ?></p>
                <p class="title"><?php the_title(); ?></p>
              </div>
            </a>
          </li>

        <?php
          endforeach;
        ?>
        </ul>
      </div>

      <div class="pagination">
        <?php the_posts_pagination(
          array(
            'mid_size'      => 2, // 現在ページの左右に表示するページ番号の数
            'prev_next'     => true, // 「前へ」「次へ」のリンクを表示する場合はtrue
            'prev_text'     => __( '＜'), // 「前へ」リンクのテキスト
            'next_text'     => __( '＞'), // 「次へ」リンクのテキスト
            'type'          => 'list', // 戻り値の指定 (plain/list)
          )
        ); ?>
      </div>

      <p class="pageTop"><a href="#"><span></span></a></p>

    </div>
  </section>

  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/contact.php'); ?>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/footer.php'); ?>
</body>
</html>