<?php
$setPath= "./";
$pageTitle = "お問い合わせ";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);

// 車台番号
if (isset($_POST['number'])) {
  $number = $_POST['number'];
} else {
  $number = '';
}

// 車名
if (isset($_POST['title_pickup'])) {
  $title_pickup = '（' . $_POST['title_pickup'] . '）';
} else {
  $title_pickup = '';
}
?>
<?php
// session_start
function init_session_start() {
  if( session_status() !== PHP_SESSION_ACTIVE ) {
    session_start();
  }
}

add_action( 'init', 'init_session_start' );
// ライブラリのcommonを呼び出す - 全フォーム共通
require_once($setPath.'lib/Mulgu/controller/Common.php');
// ライブラリのcontactを呼び出す - 対象フォーム用
require_once($setPath.'lib/Mulgu/controller/Contact.php');
// common オブジェクト
$common = new Common();
// contact オブジェクト - パラメーター収めたりする
$contact = new Contact();
$obj = $contact->form();
$params = $obj->params;
if ( $obj->has_error ) {
  $errors = $obj->errors;
}
?>

<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
<style type="text/css">
.mw_wp_form_confirm .conf-hidden {
  display: none;
}
</style>
</head>

<body class="contact">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">お問い合わせ</span><br><span class="en">CONTACT</span></h2>
    </div>
  </section>

  <section class="con01">
    <div class="wrapper">
      <h3 class="tit01">お電話でのお問い合わせ</h3>
      <div class="tel_info">
        <p class="number"><span><a href="tel:0467-87-1883">0467-87-1883</a></span></p>
        <p class="txt">営業時間：9:00〜18:00　<br class="sp-on">定休日：水曜日</p>
      </div>

      <h3 class="tit01">メールでのお問い合わせ</h3>
      <?php the_content(); ?>
    </div>
  </section>
  
  <?php require_once($setPath.'lib/include/footer.php'); ?>

  <script>
    // 同意チェック
    $(document).ready(function () {
      $(".agree input[type='checkbox']").click(function () {
        if ($(".contact-btn").hasClass('-stop')) {
          jQuery(".contact-btn").removeClass("-stop");
        } else {
          jQuery(".contact-btn").addClass("-stop");
        }
      })
    })

    // 同意チェック
    function do_consent() {
      if (jQuery(".js-consent:checked").val()) {
        jQuery(".js-confirm").removeClass("-stop");
      } else {
        jQuery(".js-confirm").addClass("-stop");
      }
    }

    jQuery('.js-consent').on('load click', function() {
      do_consent();
    });

    jQuery('.js-confirm').on('click', function() {
      do_submit(this,'contact_form');
    });
    
    // 問い合わせ
    function do_submit(a,fname) {
      if ( jQuery(".js-consent:checked").val() ) {
        document[fname].submit();
        return false;
      }
    }

    // 同意チェック
    function do_consent() {
      if ( jQuery(".js-consent:checked").val() ) {
        jQuery(".js-confirm").removeClass("-stop");
      }else{
        jQuery(".js-confirm").addClass("-stop");
      }
    }

    // 車台番号
    var number = '<?php echo $number . $title_pickup; ?>';
    $('#f_number').val(number);

  </script>

  <?php wp_footer(); ?>
</body>
</html>