<?php
$setPath= "../";
$pageTitle = "404";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/head.php'); ?>
</head>

<body class="error">
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">エラー</span><br><span class="en">404</span></h2>
    </div>
  </section>

  <section class="con01">
    <div class="wrap">
      <p class="message">404 Not Found<br>ページが見つかりませんでした。</p>
      <p class="link"><a href="/">TOPへ戻る</a></p>
    </div>
  </section>

  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/contact.php'); ?>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/footer.php'); ?>
</body>
</html>