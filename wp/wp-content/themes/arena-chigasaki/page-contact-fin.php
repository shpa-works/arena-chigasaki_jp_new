<?php
$setPath= "./";
$pageTitle = "お問い合わせ完了";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>

<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
</head>

<body class="contact thanks">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">お問い合わせ</span><br><span class="en">CONTACT</span></h2>
    </div>
  </section>

  <section class="con01">
    <div class="wrapper">
      <p class="txt">お問い合わせ<br class="sp-on">ありがとうございました</p>
      <ul class="cars">
        <li><img src="../lib/cmn-img/contact/thanks_car01.svg" alt=""></li>
        <li><img src="../lib/cmn-img/contact/thanks_car02.svg" alt=""></li>
        <li><img src="../lib/cmn-img/contact/thanks_car03.svg" alt=""></li>
      </ul>
      <div class="button"><a href="../">トップページへ</a></div>
    </div>
  </section>

  <?php require_once($setPath.'lib/include/footer.php'); ?>
  
</body>
</html>