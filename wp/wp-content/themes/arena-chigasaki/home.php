<?php
$setPath = "";
$pageTitle = "";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<?php
// session始める - 見ればわかるか
session_start();
// ライブラリのcommonを呼び出す - 全フォーム共通
require_once("lib/Mulgu/controller/Common.php");
// ライブラリのcontactを呼び出す - 対象フォーム用
require_once("lib/Mulgu/controller/Contact.php");
// common オブジェクト
$common = new Common();
// contact オブジェクト - パラメーター収めたりする
$contact = new Contact();
$obj = $contact->form();
$params = $obj->params;
if ($obj->has_error) {
  $errors = $obj->errors;
}
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath . 'lib/include/head.php'); ?>
  <link href="assets/lib/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
  <link rel="stylesheet" href="/lib/pickup/css/style.css" media="all">
</head>

<body class="home">
  <?php require_once($setPath . 'lib/include/header.php'); ?>

  <section class="mv pc-on">
    <ul class="slider">
      <li><a href="/#testlist"><img src="lib/cmn-img/index/mv_slide_testlist@2x.jpg" alt="試乗車・展示車　ラインナップ大集合"></a></li>
      <li><img src="lib/cmn-img/index/mv_slide_suzuki_pc.jpg" alt="スズキの日"></li>
      <li><a href="/#testdrive"><img src="lib/cmn-img/index/mv_slide_test_pc.jpg" alt="試乗で体感　予約フォーム"></a></li>
      <li><a href="/pickup/"><img src="lib/cmn-img/index/mv_slide_pickup@2x.jpg" alt="厳選中古車　在庫情報"></a></li>
      <li><img src="lib/cmn-img/index/mv_slide_service_pc.jpg" alt="アフターサービスも自社工場完備であんしん"></li>
    </ul>
  </section>

  <section class="mv sp-on">
    <ul class="slider">
      <li><a href="/#testlist"><img src="lib/cmn-img/index/sp/mv_slide_lineup.jpg" alt="試乗車・展示車　ラインナップ大集合"></a></li>
      <li><img src="lib/cmn-img/index/sp/mv_slide_suzuki.jpg" alt="スズキの日"></li>
      <li><a href="/#testdrive"><img src="lib/cmn-img/index/sp/mv_slide_test.jpg" alt="試乗で体感　予約フォーム"></a></li>
      <li><a href="/pickup/"><img src="lib/cmn-img/index/sp/mv_slide_used.jpg" alt="厳選中古車　在庫情報"></a></li>
      <li><img src="lib/cmn-img/index/sp/mv_slide_service.jpg" alt="アフターサービスも自社工場完備であんしん"></li>
    </ul>
  </section>

  <section class="sec01">
    <div class="wrapper">

      <div id="flyer">
        <div class="flyer_box">

          <?php
          $args = array(
            'posts_per_page' => 1,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'chirashi',
            'post_status' => 'publish'
          );

          $the_query = new WP_Query($args);

          // チラシのループスタート
          if ($the_query->have_posts()) :
            while ($the_query->have_posts()) : $the_query->the_post();

              $f_chirashi_omote_photo = get_field('f_chirashi_omote_photo'); // 表（画像）
              $f_chirashi_omote_file  = get_field('f_chirashi_omote_file');  // 表（PDF）
              $f_chirashi_ura_photo   = get_field('f_chirashi_ura_photo');   // 裏（画像）
              $f_chirashi_ura_file    = get_field('f_chirashi_ura_file');    // 裏（PDF）

              $omote_photo = 'lib/cmn-img/index/comingsoon.jpg';
              if (!is_bool($f_chirashi_omote_photo)) {
                $omote_photo = $f_chirashi_omote_photo;
              }

              $omote_file = '';
              if (!is_bool($f_chirashi_omote_file)) {
                $omote_file = $f_chirashi_omote_file;
              }

              $ura_photo = 'lib/cmn-img/index/comingsoon.jpg';
              if (!is_bool($f_chirashi_ura_photo)) {
                $ura_photo = $f_chirashi_ura_photo;
              }

              $ura_file = '';
              if (!is_bool($f_chirashi_ura_file)) {
                $ura_file = $f_chirashi_ura_file;
              }
          ?>

            <?php if ($omote_file == '') { ?>
              <div class="flyer">
                <img src="<?php echo $omote_photo; ?>" alt="チラシ">
              </div>
            <?php } else { ?>
              <div class="flyer">
                <a href="<?php echo $omote_file; ?>" target="_blank">
                  <img src="<?php echo $omote_photo; ?>" alt="チラシ">
                </a>
              </div>
            <?php } ?>

            <?php if ($ura_file !== '') { ?>
              <div class="flyer">
                <a href="<?php echo $ura_file; ?>" target="_blank">
                  <img src="<?php echo $ura_photo; ?>" alt="チラシ">
                </a>
              </div>
            <?php } ?>

          <?php
            endwhile;
          endif;

          // チラシのループ終わり
          wp_reset_postdata();
          ?>

        </div>
      </div>

      <h2 class="tit">PICK UP<img src="lib/cmn-img/index/img_pickup_text.svg" alt="気になる情報はこちらから!!"></h2>
      <div class="bnr_box_outline">
        <div class="bnr_box clearfix">
          <div class="bnr"><a href="/#testdrive"><img src="lib/cmn-img/index/pickup_testdrive.png" alt="試乗車ご予約へ"></a></div>
          <div class="bnr"><a href="/pickup/"><img src="lib/cmn-img/index/pickup_pickup.png" alt="厳選中古車在庫情報へ"></a></div>
          <div class="bnr"><a href="https://www.carsensor.net/shop/kanagawa/307926002/#contents" target="_blank"><img src="lib/cmn-img/index/pickup_carsensor.png" alt="カーセンサー在庫検索"></a></div>
        </div>
      </div>

      <?php
      $count_posts = wp_count_posts();
      $num = $count_posts->publish; // 公開記事のカウント数取得

      if ($num != 0) { // 1件以上あれば表示
        echo '<div class="news">';
        echo '  <p class="tit">新着情報</p>';
        echo '    <div class="txt">';
        echo '      <ul>';

        $args = array(
          'posts_per_page' => 3 // 表示件数の指定
        );
        $posts = get_posts($args);
        foreach ($posts as $post) : // ループの開始
          setup_postdata($post); // 記事データの取得
      ?>
          <li>
            <a href="<?php the_permalink(); ?>">
              <span class="date"><?php echo get_the_date('Y.m.d'); ?></span>
              <span class="title"><?php the_title(); ?></span>
            </a>
          </li>
      <?php
        endforeach; // ループの終了

        echo '      </ul>';
        echo '    </div>';
        echo '  <p class="more"><a href="/news/">もっと見る<span class="arrow"></span></a></p>';
        echo '</div>';
      }
      ?>

    </div>
  </section>

  <section class="sec02">
    <div class="wrapper">
      <h2 class="inview fadeIn_up">
        <span>湘南エリアで<br class="sp-on">新車・中古車をお探しの方</span><br>
        スズキアリーナ茅ヶ崎へ<br class="sp-on">お越しください！
      </h2>
      <p class="message">車の購入や点検整備・自動車保険のご相談は弊社にお任せください！<br>JR茅ケ崎駅より徒歩５分、国道１号線沿いのお店です！<br>試乗車のラインナップも豊富です！試乗車に販売店オプション多数装備済です！<br>人気のオプションを実際に装備しておりますので現物確認が出来ます！</p>
    </div>
  </section>

  <section id="selectcar">
    <div class="selectcar-inner">
      <h2 class="tit01">
        <span class="jp">厳選中古車</span><br>
        <span class="en">SELECT CAR</span>
      </h2>

      <div class="carlist">
        <ul>

          <?php
          $args = array(
            'posts_per_page' => 6,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'post_type' => 'pickup',
            'post_status' => 'publish'
          );

          $the_query = new WP_Query($args);

          // 記事一覧のループスタート
          if ($the_query->have_posts()) :
            while ($the_query->have_posts()) : $the_query->the_post();

              $f_grade = get_field('f_grade'); // グレード
              $f_price_body = get_field('f_price_body'); // 車両本体価格(税込)
              $f_price_pay = get_field('f_price_pay'); // 支払総額(税込)
              $f_year = get_field('f_year'); // 年式
              $f_number = get_field('f_number'); // 車台番号
              $f_safety = get_field('f_safety'); // 車検有無
              $f_range = get_field('f_range'); // 走行距離
              $f_color = get_field('f_color'); // ボディカラー
              $f_option = get_field('f_option'); // オプション
              $f_photo_main = get_field('f_photo_main'); // 画像（メイン）
              $f_check = get_field('f_check'); // 整備
              $f_repair = get_field('f_repair'); // 修復歴
              $f_security = get_field('f_security'); // 保証
              $f_notes = get_field('f_notes'); // 補足
          ?>

              <li>
                <a href="<?php the_permalink(); ?>">
                  <div class="carlist-inner">
                    <div class="carlist-photo"><img src="<?php echo $f_photo_main ?>" alt="<?php the_title(); ?>"></div>
                    <div class="carlist-title">
                      <p class="carlist-title-name"><?php the_title(); ?></p>
                      <p class="carlist-title-grade"><?php echo $f_grade; ?></p>
                    </div>
                    <dl class="carlist-price">
                      <dt>支払総額(税込)</dt>
                      <dd class="red"><span><?php echo $f_price_pay; ?></span>万円</dd>
                    </dl>
                    <div class="carlist-notes">
                      <dl>
                        <dt>車両価格</dt>
                        <dd><?php echo $f_price_body; ?>万円（税込）</dd>
                        <dt>諸費用</dt>
                        <dd>
                          <?php
                          // 支払総額 - 車両価格 = 諸費用
                          if ($f_price_pay != '' && $f_price_body != '') {
                            $overhead = floatval($f_price_pay) - floatval($f_price_body);
                            echo $overhead . '万円（税込）';
                          }
                          ?>
                        </dd>
                      </dl>
                      <p class="security">
                        <?php
                        $out_check = '';
                        if ($f_check == '定期点検整備つき') {
                          $out_check = 'あり';
                        } else {
                          $out_check = 'なし';
                        }

                        $out_security = '';
                        if ($f_security == '保証付') {
                          $out_security = '（' . $f_notes . '）';
                        }

                        $out_repair = '';
                        if ($f_repair == '修復歴あり') {
                          $out_repair = '／修復歴有り';
                        } else {
                          $out_repair = '／修復歴無し';
                        }
                        echo '法定整備' . $out_check . '／' . $f_security . $out_security . $out_repair;
                        ?>
                      </p>
                    </div>
                    <dl class="carlist-details">
                      <dt>年式</dt>
                      <dd><?php echo $f_year; ?></dd>
                      <dt>車台番号</dt>
                      <dd><?php echo $f_number; ?></dd>
                      <dt>車検有無</dt>
                      <dd><?php echo $f_safety; ?></dd>
                      <dt>走行距離</dt>
                      <dd><?php echo $f_range; ?></dd>
                      <dt>ボディカラー</dt>
                      <dd><?php echo $f_color; ?></dd>
                    </dl>
                    <p class="carlist-link"><span>詳しくみる</span></p>
                  </div>
                </a>
              </li>

          <?php
            endwhile;
          endif;

          // 記事一覧のループ終わり
          wp_reset_postdata();
          ?>

        </ul>
      </div>
      <div class="btn-block"><a href="/pickup/"><span>在庫一覧ページへ</span></a></div>
    </div>
  </section>

  <section class="sec06">
    <div class="wrapper clearfix">
      <div class="box instagram">
        <p>新車の紹介やお店の情報など、<br>Instagramでお届けしています！</p>
        <div class="icon"><img src="lib/cmn-img/index/instagram.svg" alt="Instagram"></div>
        <div class="img"><img src="lib/cmn-img/index/instagram@2x.png" alt="インスタグラム"></div>
        <div class="button">
          <a href="https://www.instagram.com/suzukiarena.chigasaki/" target="_blank">InstagramをCheck!</a>
        </div>
      </div>
      <div class="box carsensor">
        <p>スズキアリーナ茅ヶ崎では<br>中古車も多数取り揃えております！</p>
        <div class="img"><img src="lib/cmn-img/index/carsensor_logo@2x.png" alt="カーセンサー"></div>
        <div class="button">
          <a href="https://www.carsensor.net/shop/kanagawa/307926002/#contents" target="_blank">中古車検索はこちら</a>
        </div>
      </div>
    </div>
  </section>

  <section class="sec03">
    <div class="wrapper">
      <div class="cont-box">
        <h2 class="inview fadeIn_up"><span>スズキアリーナ茅ヶ崎は試乗だけでも大歓迎！！</span></h2>
        <div class="step_wrap clearfix">
          <div class="step">
            <div class="number"><img src="lib/cmn-img/index/step01@2x.png" alt="STEP01"></div>
            <div class="img">
              <img class="pc-on inview fade01" src="lib/cmn-img/index/step01_img@2x.png" alt="STEP01">
              <img class="sp-on inview fade01" src="lib/cmn-img/index/sp/step01_img@2x.png" alt="STEP01">
            </div>
            <p class="txt01">ページ下部の予約フォームより必須事項を入力ください。</p>
          </div>
          <div class="step">
            <div class="number"><img src="lib/cmn-img/index/step02@2x.png" alt="STEP02"></div>
            <div class="img">
              <img class="pc-on inview fade02" src="lib/cmn-img/index/step02_img@2x.png" alt="STEP02">
              <img class="sp-on inview fade02" src="lib/cmn-img/index/sp/step02_img@2x.png" alt="STEP02">
            </div>
            <p class="txt01">3営業日以内に予約日程の確認のご連絡をいたします。</p>
            <p class="txt02">※長期連休の際はご容赦ください。</p>
          </div>
          <div class="step">
            <div class="number"><img src="lib/cmn-img/index/step03@2x.png" alt="STEP03"></div>
            <div class="img">
              <img class="pc-on inview fade03" src="lib/cmn-img/index/step03_img@2x.png" alt="STEP03">
              <img class="sp-on inview fade03" src="lib/cmn-img/index/sp/step03_img@2x.png" alt="STEP03">
            </div>
            <p class="txt01">試乗当日は受付の際に運転免許証のご確認があります。</p>
            <p class="txt02">※お車をお持ちの方は店舗内の駐車場をご利用ください。<br>※当日のお持物は普通自動車免許だけでOK</p>
          </div>
          <div class="step">
            <div class="number"><img src="lib/cmn-img/index/step04@2x.png" alt="STEP04"></div>
            <div class="img">
              <img class="pc-on inview fade04" src="lib/cmn-img/index/step04_img@2x.png" alt="STEP04">
              <img class="sp-on inview fade04" src="lib/cmn-img/index/sp/step04_img@2x.png" alt="STEP04">
            </div>
            <p class="txt01">試乗開始！</p>
          </div>
        </div>
      </div>

      <div class="cont-box">
        <h2 class="inview fadeIn_up"><span>試乗方法は2種類よりお選びいただけます！</span></h2>

        <div class="box_wrap clearfix">
          <div class="box inview fadeIn_up01">
            <p class="tit"><span>スタンダードコース</span></p>
            <p class="txt">スタッフが同乗のうえ、車の説明をさせていただきます。<br>ご質問など、お気軽にお声掛けください！</p>
            <div><img src="lib/cmn-img/index/course01@2x.jpg" alt="スタンダードコース"></div>
          </div>

          <div class="box inview fadeIn_up02">
            <p class="tit"><span>セルフコース</span></p>
            <p class="txt">ご試乗前に車両の基本操作をご案内いたします。<br>日常に一番近い形での試乗が出来る、人気のコースです！</p>
            <div><img src="lib/cmn-img/index/course02@2x.jpg" alt="セルフコース"></div>
          </div>
        </div>

        <p class="text" style="display: none;">※新型コロナ対策としまして、試乗に同乗される人数によりセルフコースのみのご案内になる場合がございます。</p>
      </div>
    </div>
  </section>

  <section class="sec04" id="testlist">
    <div class="wrapper">
      <h2 class="tit01">
        <span class="jp">試乗車・展示車</span><br>
        <span class="en">TEST DRIVE / EXHIBITION CAR</span>
      </h2>


      <div class="tab-panel">
        <ul class="tab-group clearfix">
          <li class="tab tab-A is-active">小型・乗用車</li>
          <li class="tab tab-B">軽自動車</li>
        </ul>

        <ul class="badges">
          <li class="new" style="display: none;">最新モデル</li>
          <li class="eco" style="display: none;">エコカー</li>
          <li class="suv" style="display: none;">SUVタイプ</li>
        </ul>

        <div class="panel-group">
          <div class="panel tab-A is-show_panel clearfix">						

            <div class="box height">
              <p class="tit">ソリオ</p>
              <div class="img"><img src="lib/cmn-img/index/car57.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">HYBRID MZ<br>全方位モニター付メモリーナビゲーション<br>スズキコネクト対応通信機装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">ピュアホワイトパール（ZVR）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">ソリオバンディット</p>
              <div class="img"><img src="lib/cmn-img/index/car58.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">HYBRID MV<br>全方位モニター付メモリーナビゲーション<br>スズキコネクト対応通信機装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">スーパーブラックパール（ZMV）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">フロンクス</p>
              <div class="img"><img src="lib/cmn-img/index/car50.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt"></p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/6AT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">アークティックホワイトパール<br>ブラック２トーンルーフ（E5G）</p>
              </div>
            </div>

						<div class="box height">
              <p class="tit">スイフトスポーツ</p>
              <div class="img"><img src="lib/cmn-img/index/car52.jpg" alt=""></div>
              <div class="txtwrap">
								<p class="mintit">グレード</p>
                <p class="txt"></p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/6AT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">スピーディーブルーメタリックブラック2トーンルーフ（C7R）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">ジムニーシエラ</p>
              <div class="img"><img src="lib/cmn-img/index/car41@2x.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">JC</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">4WD/4AT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">ミディアムグレー（ZVL）</p>
              </div>
            </div>
						
						

          </div>



          <div class="panel tab-B clearfix">

            <div class="box height">
              <p class="tit">ワゴンＲスマイル</p>
              <div class="img"><img src="lib/cmn-img/index/car59.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">HYBRID X<br>全方位モニター付メモリーナビゲーション<br>スズキコネクト対応通信機装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">トープグレージュメタリック<br>ソフトベージュ２トーンルーフ（FC7）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">ジムニー</p>
              <div class="img"><img src="lib/cmn-img/index/car51.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">XC</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">4WD/4AT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">ジャングルグリーン（ZZC）</p>
              </div>
              <ul class="badge" style="display: none;">
                <li><img src="lib/cmn-img/ico/ico-badge_new.svg" alt=""></li>
              </ul>
            </div>

            <div class="box height">
              <p class="tit">ラパンLC</p>
              <div class="img"><img src="lib/cmn-img/index/car48.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">X バックアイカメラ付ディスプレイオーディオ装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">ノクターンブルーパール<br>ソフトベージュ２トーンルーフ（EYA）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">スペーシア</p>
              <div class="img"><img src="lib/cmn-img/index/car55.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">HYBRID X<br>全方位モニター付メモリーナビゲーション<br>スズキコネクト対応通信機装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">ピュアホワイトパール（ZVR）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">スペーシアカスタム</p>
              <div class="img"><img src="lib/cmn-img/index/car56.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">HYBRID XS TURBO<br>全方位モニター付メモリーナビゲーション<br>スズキコネクト対応通信機装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">インディゴブルーメタリック２<br>ブラック２トーンルーフ（EL8）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">スペーシアギア</p>
              <div class="img"><img src="lib/cmn-img/index/car49.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">HYBRID XZ ターボ<br>全方位モニター付メモリーナビゲーション<br>スズキコネクト対応通信機装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">オフブルーメタリック<br>ガンメタリック２トーンルーフ（DYA）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">エブリイワゴン</p>
              <div class="img"><img src="lib/cmn-img/index/car47.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">PZターボスペシャル　標準ルーフ<br>バックアイカメラ付ディスプレイオーディオ装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">モスグレーメタリック（WBW）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">ハスラー</p>
              <div class="img"><img src="lib/cmn-img/index/car40@2x.jpg" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">タフワイルドターボ<br>全方位モニター付メモリーナビゲーション<br>スズキコネクト対応通信機装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">クールカーキパールメタリック（ZVD）</p>
              </div>
            </div>

            <div class="box height">
              <p class="tit">アルト</p>
              <div class="img"><img src="lib/cmn-img/index/car38@2x.png" alt=""></div>
              <div class="txtwrap">
                <p class="mintit">グレード</p>
                <p class="txt">HYBRID X<br>全方位モニター付ディスプレイオーディオ装着車</p>
                <p class="mintit">駆動/ミッション</p>
                <p class="txt">2WD/CVT</p>
                <p class="mintit">ボディカラー</p>
                <p class="txt">ダスクブルーメタリック<br>ホワイト２トーンルーフ（EMB）</p>
              </div>
            </div>            
						
          </div>

          <p class="btmtxt">※グレード名、ボディカラー名の後に「※」がある車両は、最新のモデル及びボディーカラーではありません。<br>※試乗車は予告なく変更することがあります。 確実に車に触れていただくために、事前にお店へご確認の上、ご来店ください。</p>
        </div>
      </div>
    </div>
  </section>

  <section class="sec05" id="testdrive">
    <div class="wrapper">
      <h2 class="tit01">
        <span class="jp">試乗予約フォーム</span><br>
        <span class="en">FORM</span>
      </h2>


      <form class="contact-form form -input h-adr" name="contact-form" id="contact-form" action="<?php echo $contact->path["confirm"]; ?>" method="POST">
        <input type="hidden" name="token" value="<?php echo $common->mlg->csrf->generate() ?>">

        <p><span class="kome">＊</span>は入力必須項目です。</p>
        <table>


          <tr class="contact-table-wrap<?php if ($errors['type']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -required required"><span>試乗車情報（希望車種）</span></th>
            <td class="contact-table-cont">
              <div class="selectbox">
                <?php echo $contact->mlg->tools->make_select_form($params, $contact->mlg->master->type, 'type', "text"); ?>
                <?php if ($errors['type']) { ?></div>
              <p class="-error-message">
                <?php echo $errors['type']; ?>
              </p>
            <?php } ?>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['date']) { ?> -error<?php } ?>">
            <th class="required contact-table-ttl -any"><span>試乗希望日時</span></th>
            <td class="contact-table-cont clearfix">
              <div class="detetime">
                <div class="datebox">
                  <input name="date" id="datetimepicker1" type="text" class="txt01" placeholder="選択して下さい" autocomplete="off">
                </div>
                <?php if ($errors['date']) { ?>
                  <p class="-error-message"><?php echo $errors['date']; ?></p>
                <?php } ?>
              </div>

              <div class="detetime">
                <div class="timebox">
                  <input name="time" id="datetimepicker2" type="text" class="txt01" placeholder="選択して下さい" autocomplete="off">
                </div>
                <?php if ($errors['time']) { ?>
                  <p class="-error-message"><?php echo $errors['time']; ?></p>
                <?php } ?>
              </div>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['name']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -required required"><span>お名前</span></th>
            <td class="contact-table-cont"><input type="text" name="name" id="name" value="<?php echo $params['name']; ?>" class="-input -text txt02">
              <?php if ($errors['name']) { ?>
                <p class="-error-message">
                  <?php echo $errors['name']; ?>
                </p>
              <?php } ?>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['kana']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -required required"><span>ふりがな</span></th>
            <td class="contact-table-cont"><input type="text" name="kana" id="kana" value="<?php echo $params['kana']; ?>" class="-input -text txt02">
              <?php if ($errors['kana']) { ?>
                <p class="-error-message">
                  <?php echo $errors['kana']; ?>
                </p>
              <?php } ?>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['phone']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -required required"><span>日中連絡がつきやすい<br class="pc-on">お電話番号</span></th>
            <td class="contact-table-cont">
              <input type="text" name="phone" id="phone" value="<?php echo $params['phone']; ?>" class="-input -text txt02">
              <?php if ($errors['phone']) { ?>
                <p class="-error-message">
                  <?php echo $errors['phone']; ?>
                </p>
              <?php } ?>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['email']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -required required"><span>メールアドレス</span></th>
            <td class="contact-table-cont"><input type="text" name="email" id="email" value="<?php echo $params['email']; ?>" class="-input -text txt02">
              <?php if ($errors['email']) { ?>
                <p class="-error-message">
                  <?php echo $errors['email']; ?>
                </p>
              <?php } ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>メールアドレス（確認用）</span></th>
            <td class="contact-table-cont">
              <input class="-input txt02" name="email_double" type="text" value="<?php echo $params['email_double']; ?>" style="ime-mode:disabled;">
              <?php if ($errors['email_double']) { ?>
                <p class="-error-message">
                  <?php echo $errors['email_double']; ?>
                </p>
              <?php } ?>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['code']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -any">郵便番号</th>
            <td class="contact-table-cont">〒<input type="text" name="code" id="code" value="<?php echo $params['code']; ?>" class="-input -text txt03 p-postal-code">
              <div class="code-text">ハイフンを入れずに入力してください<br>▼郵便番号を入力すると、住所の一部が自動的に表示されます</div>
            </td>
            <input type="hidden" class="p-country-name" value="Japan">
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['address']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -any">ご住所</th>
            <td class="contact-table-cont"><input type="text" name="address" id="address" value="<?php echo $params['address']; ?>" class="-input -text txt02 p-region p-locality p-street-address p-extended-address"></td>
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['people']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -required required"><span>来店予定人数</span></th>
            <td class="contact-table-cont"><input type="text" name="people" id="people" value="<?php echo $params['people']; ?>" class="-input -text txt03"> 人
              <?php if ($errors['people']) { ?>
                <p class="-error-message">
                  <?php echo $errors['people']; ?>
                </p>
              <?php } ?>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['contact_type']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl"><span>ご希望の連絡方法</span></th>
            <td class="contact-table-cont">
              <?php echo $contact->mlg->tools->make_radio_form($params, $contact->mlg->master->contact_type, 'contact_type', "text"); ?>
            </td>
          </tr>


          <tr class="contact-table-wrap<?php if ($errors['comment']) { ?> -error<?php } ?>">
            <th class="contact-table-ttl -any">その他ご質問・ご要望</th>
            <td class="contact-table-cont"><textarea name="comment" class="-textarea"><?php echo $params['comment']; ?></textarea></td>
          </tr>
        </table>


        <div class="agree">
          <p>「<a href="privacypolicy" target="_blank">プライバシーポリシー</a>」をご一読の上、内容に同意いただける場合は「同意する」にチェックし、内容確認画面にお進みください。</p>
          <div class="contact-consent check">
            <label><input type="checkbox" name="is_checked" class="-checked js-consent" id="-checked" value="t">同意する</label>
          </div>
        </div>
        <div class="contact-btn">
          <a href="javascript:void(0)" onclick="do_submit(this,'contact-form');return false;" class="contact-btn-item -btn01 -stop js-confirm">確認画面へ</a>
        </div>
      </form>
    </div>
  </section>

<?php /*
  <section class="sec07" style="display: none;">
    <div class="wrapper">
      <h2>新型コロナウィルス感染防止対応策について</h2>
      <p class="mintit">お客様に安心してご来店いただくために、当店ではこんな対策をしております。</p>
      <div class="boxwrap clearfix">
        <div class="box height clearfix inview fade01">
          <div class="icon icon01"><img src="lib/cmn-img/ico/ico-COVID19_01.svg" alt=""></div>
          <div class="txtbox">
            <p class="tit">店内換気の徹底</p>
            <p class="txt">営業中はこまめに換気を行っております。</p>
          </div>
        </div>
        <div class="box height clearfix inview fade02">
          <div class="icon icon02"><img src="lib/cmn-img/ico/ico-COVID19_02.svg" alt=""></div>
          <div class="txtbox">
            <p class="tit">スタッフのマスク着用</p>
            <p class="txt">全スタッフマスクを着用し、ご対応させていただきます。</p>
          </div>
        </div>
        <div class="box height clearfix inview fade03">
          <div class="icon icon03"><img src="lib/cmn-img/ico/ico-COVID19_03.svg" alt=""></div>
          <div class="txtbox">
            <p class="tit">お客様用消毒液の設置</p>
            <p class="txt">ご来店の際は、入口で手指の消毒をお願いしております。</p>
          </div>
        </div>
        <div class="box height clearfix inview fade04">
          <div class="icon icon04"><img src="lib/cmn-img/ico/ico-COVID19_04.svg" alt=""></div>
          <div class="txtbox">
            <p class="tit">定期的な店内の消毒</p>
            <p class="txt">ショールームのテーブル・椅子・トイレなどを共有部分を定期的に除菌・消毒しております。</p>
          </div>
        </div>
      </div>
      <div class="message">
        <div class="tit">
          <p>お客様へのお願い</p>
        </div>
        <div class="txtbox">
          <p class="txt01">ご来店の際は、マスクの着用・入口での手指の消毒・咳エチケットの徹底に<br>ご協力をお願い致します。</p>
          <p class="txt02">※発熱症状のある方、体調が優れない方はご来店をお控えいただきますようお願い致します。</p>
        </div>
      </div>
    </div>
  </section>
*/ ?>

  <section id="pricelist">
    <div class="wrapper">
      <h2 class="tit01">
        <span class="jp">定期点検標準技術料金表</span><br>
        <span class="en">PRICE LIST
      </h2>
      <div class="chart">
        <a href="/lib/cmn-img/index/pricelist_240705.pdf" target="_blank">
          <img src="/lib/cmn-img/index/pricelist_240705.jpg" alt="定期点検標準技術料金表（自家用）">
        </a>
      </div>
    </div>
  </section>

  <?php require_once($setPath . 'lib/include/contact.php'); ?>
  <?php require_once($setPath . 'lib/include/footer.php'); ?>

  <script src="lib/cmn-js/yubinbango.js"></script>
  <script src="assets/lib/datetimepicker/jquery.datetimepicker.full.min.js"></script>

  <script>
    $(function() {
      $.datetimepicker.setLocale('ja'); // 日本語化

      $('#datetimepicker1').datetimepicker({
        timepicker: false,
        format: "y/m/d",
        formatDate: "y/m/d",
        disabledWeekDays: [3],
        disabledDates: ['24/12/27', '24/12/28', '24/12/29', '24/12/30', '24/12/31', '25/01/02', '25/01/03']
      });
    });


    $(function() {
      $('#datetimepicker2').datetimepicker({
        datepicker: false,
        allowTimes: [
          "09:30", "10:00", "10:30", "11:00", "11:30",
          "13:00", "13:30", "14:00", "14:30", "15:00",
          "15:30", "16:00", "16:30", "17:00", "17:30"
        ],
        format: 'H:i'
      });
    });
  </script>

  <script>
    // 同意チェック
    function do_consent() {
      if (jQuery(".js-consent:checked").val()) {
        jQuery(".js-confirm").removeClass("-stop");
      } else {
        jQuery(".js-confirm").addClass("-stop");
      }
    }

    jQuery('.js-consent').on('load click', function() {
      do_consent();
    });

    jQuery('.js-confirm').on('click', function() {
      do_submit(this, 'contact_form');
    });

    // 問い合わせ
    function do_submit(a, fname) {
      if (jQuery(".js-consent:checked").val()) {
        document[fname].submit();
        return false;
      }
    }

    // 同意チェック
    function do_consent() {
      if (jQuery(".js-consent:checked").val()) {
        jQuery(".js-confirm").removeClass("-stop");
      } else {
        jQuery(".js-confirm").addClass("-stop");
      }
    }
  </script>

</body>

</html>