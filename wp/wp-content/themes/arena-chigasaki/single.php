<?php
$setPath= "../";
$category = get_the_category(); 
$cat_name = $category[0]->name;
$the_title = get_the_title();
$pageTitle = "【" . $cat_name . "】" . $the_title;

$description = $post->post_content;
$description = str_replace(array("\r\n","\r","\n","&nbsp;"),'',$description);
$description = wp_strip_all_tags($description);
$description = preg_replace('/\[.*\]/','',$description);
$description = mb_strimwidth($description,0,162,"…");

$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => $description,
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/head.php'); ?>
</head>

<body class="news">
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">ブログ</span><br><span class="en">BLOG</span></h2>
    </div>
  </section>

  <section class="con01">
    <div class="wrap">
      <div class="pageBack"><a href="/news/"><span>記事一覧に戻る</span></a></div>

      <?php
        $category = get_the_category(); 
        $cat_name = $category[0]->name;
        $cat_slug = $category[0]->slug;
      ?>
      <div class="page-category">
        <span class="<?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span>
      </div>

      <section class="page-content">
        <div class="page-wrap">
          <div class="container">
            <p class="page-title"><?php the_title(); ?></p>
            <div class="page-photo"><?php the_post_thumbnail(); ?></div>
            <div class="page-body">
              <?php the_content(); ?>
            </div>
          </div>
        </div>

        <p class="pageTop"><a href="#"><span></span></a></p>

        <div class="page-relation">
          <p class="relation-title">関連記事</p>
        </div>

        <div class="newsArea">
          <ul>

            <?php 
              $post_id = get_the_ID();
              $cat = get_the_category(); 
              $cat = $cat[0];

              $cate_name = $cat->name;
              $cate_slug = $cat->slug;
              $args = [
                'category_name' => $cate_slug, // カテゴリを絞る
              ];

              $count = 0;
              $custom_posts = get_posts($args);
              foreach ( $custom_posts as $post ): setup_postdata($post);
                if ($post_id !== $post->ID && $count < 3) {
                  $count++;
            ?>

              <li>
                <a href="<?php the_permalink(); ?>">
                  <div class="photoArea">
                    <p class="photo">
                    <?php if (has_post_thumbnail()): ?>
                      <?php the_post_thumbnail(); ?>
                    <?php else: ?>
                      <img src="/lib/cmn-img/news/dummy.jpg" alt="">
                    <?php endif; ?>
                    </p>
                    <p class="category <?php echo $cate_slug; ?>"><?php echo $cate_name; ?></p>
                  </div>
                  <div class="detailArea">
                    <p class="date"><?php the_time('Y/m/d') ?></p>
                    <p class="title"><?php the_title(); ?></p>
                  </div>
                </a>
              </li>

            <?php
                }
              endforeach;
            ?>

          </ul>
        </div>
      </section>

    </div>
  </section>
  
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/contact.php'); ?>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/footer.php'); ?>
</body>
</html>