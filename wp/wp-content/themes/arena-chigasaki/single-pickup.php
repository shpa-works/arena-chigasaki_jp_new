<?php
$setPath= "../";
$pageTitle = get_the_title();
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => $description,
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/head.php'); ?>
  <link rel="stylesheet" href="/lib/pickup/css/style.css" media="all">
</head>

<body id="pickup" class="page">
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">厳選中古車</span><br><span class="en">SELECT CAR</span></h2>
    </div>
  </section>

  <?php
    $f_grade = get_field('f_grade'); // グレード
    $f_price_body = get_field('f_price_body'); // 車両本体価格(税込)
    $f_price_pay = get_field('f_price_pay'); // 支払総額(税込)
    $f_year = get_field('f_year'); // 年式
    $f_number = get_field('f_number'); // 車台番号
    $f_safety = get_field('f_safety'); // 車検有無
    $f_range = get_field('f_range'); // 走行距離
    $f_color = get_field('f_color'); // ボディカラー
    $f_option = get_field('f_option'); // オプション
    $f_photo_main = get_field('f_photo_main'); // 画像（メイン）
    $f_photo_sub_01 = get_field('f_photo_sub_01'); // 画像（サブ1）
    $f_photo_sub_02 = get_field('f_photo_sub_02'); // 画像（サブ2）
    $f_photo_sub_03 = get_field('f_photo_sub_03'); // 画像（サブ3）
    $f_photo_sub_04 = get_field('f_photo_sub_04'); // 画像（サブ4）
    $f_photo_sub_05 = get_field('f_photo_sub_05'); // 画像（サブ5）
    $f_photo_sub_06 = get_field('f_photo_sub_06'); // 画像（サブ6）
    $f_photo_sub_07 = get_field('f_photo_sub_07'); // 画像（サブ7）
    $f_photo_sub_08 = get_field('f_photo_sub_08'); // 画像（サブ8）
    $f_photo_sub_09 = get_field('f_photo_sub_09'); // 画像（サブ9）
    $f_check = get_field('f_check'); // 整備
    $f_repair = get_field('f_repair'); // 修復歴
    $f_security = get_field('f_security'); // 保証
    $f_notes = get_field('f_notes'); // 補足
  ?>

  <section class="conts">
    <div class="wrap">
      <div class="pageBack"><a href="/pickup/"><span>一覧に戻る</span></a></div>

      <h3 class="page-title"><?php echo get_the_title(); ?></h3>

      <div class="page-block">
        <div class="block-photo">
          <ul class="main-slider">
            <?php if ($f_photo_main != '') { ?><li><img src="<?php echo $f_photo_main; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_01 != '') { ?><li><img src="<?php echo $f_photo_sub_01; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_02 != '') { ?><li><img src="<?php echo $f_photo_sub_02; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_03 != '') { ?><li><img src="<?php echo $f_photo_sub_03; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_04 != '') { ?><li><img src="<?php echo $f_photo_sub_04; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_05 != '') { ?><li><img src="<?php echo $f_photo_sub_05; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_06 != '') { ?><li><img src="<?php echo $f_photo_sub_06; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_07 != '') { ?><li><img src="<?php echo $f_photo_sub_07; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_08 != '') { ?><li><img src="<?php echo $f_photo_sub_08; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_09 != '') { ?><li><img src="<?php echo $f_photo_sub_09; ?>" alt="写真"></li><?php } ?>
          </ul>

          <ul class="thumbnail-slider">
            <?php if ($f_photo_main != '') { ?><li><img src="<?php echo $f_photo_main; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_01 != '') { ?><li><img src="<?php echo $f_photo_sub_01; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_02 != '') { ?><li><img src="<?php echo $f_photo_sub_02; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_03 != '') { ?><li><img src="<?php echo $f_photo_sub_03; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_04 != '') { ?><li><img src="<?php echo $f_photo_sub_04; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_05 != '') { ?><li><img src="<?php echo $f_photo_sub_05; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_06 != '') { ?><li><img src="<?php echo $f_photo_sub_06; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_07 != '') { ?><li><img src="<?php echo $f_photo_sub_07; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_08 != '') { ?><li><img src="<?php echo $f_photo_sub_08; ?>" alt="写真"></li><?php } ?>
            <?php if ($f_photo_sub_09 != '') { ?><li><img src="<?php echo $f_photo_sub_09; ?>" alt="写真"></li><?php } ?>
          </ul>
        </div>
        <div class="block-list">
          <dl>
            <dt>グレード</dt>
            <dd><?php echo $f_grade; ?></dd>
            <dt>支払総額(税込)</dt>
            <dd class="bold red"><span><?php echo $f_price_pay; ?></span>万円</dd>
          </dl>
          <div class="carlist-notes">
            <div class="carlist-notes-info">
              <div class="title">車両価格</div>
              <div class="body"><?php echo $f_price_body; ?>万円（税込）</div>
              <div class="title">諸費用</div>
              <div class="body">
                <?php
                  // 支払総額 - 車両価格 = 諸費用
                  if ($f_price_pay != '' && $f_price_body != '') {
                    $overhead = floatval($f_price_pay) - floatval($f_price_body);
                    echo $overhead . '万円（税込）';
                  }
                ?>
              </div>
            </div>
            <p class="security">
              <?php
                $out_check = '';
                if ($f_check == '定期点検整備つき') {
                  $out_check = 'あり';
                } else {
                  $out_check = 'なし';
                }

                $out_security = '';
                if ($f_security == '保証付') {
                  $out_security = '（' . $f_notes . '）';
                }

                $out_repair = '';
                if ($f_repair == '修復歴あり') {
                  $out_repair = '／修復歴有り';
                } else {
                  $out_repair = '／修復歴無し';
                }
                echo '法定整備' . $out_check . '／' . $f_security . $out_security . $out_repair;
              ?>
            </p>
          </div>
          <dl>
            <dt>年式</dt>
            <dd><?php echo $f_year; ?></dd>
            <dt>車台番号</dt>
            <dd><?php echo $f_number; ?></dd>
            <dt>車検有無</dt>
            <dd><?php echo $f_safety; ?></dd>
            <dt>走行距離</dt>
            <dd><?php echo $f_range; ?></dd>
            <dt>ボディカラー</dt>
            <dd><?php echo $f_color; ?></dd>
            <dt>オプション</dt>
            <dd><?php echo $f_option; ?></dd>
          </dl>
        </div>
      </div>

      <div class="notes-block">◆車体番号の下3桁を表示しております※支払総額には、車両価格の他、自賠責保険料、税金（法定費用含む）、登録等に伴う費用（検査登録手続代行費用、車庫証明手続代行費用）、リサイクル預託金相当、購入時に必要な全ての費用が含まれています。<br>
        ※支払総額は、<?php echo date('Y'); ?>年<?php echo date('n'); ?>月現在、神奈川県内登録（届出）及び店頭で納車した場合の価格です。お客様の要望に基づくオプション等の費用は別途申し受けます。</div>
      <div class="bnr-block"><a href="/contact/"><img src="/lib/pickup/img/bnr_carlife.jpg" alt=""></a></div>

      <form action="/contact/" method="post" class="form_stock">
        <div class="btn-block">
          <button type="submit"><span>在庫を問い合わせる</span></button>
        </div>
        <input type="hidden" name="number" value="<?php echo $f_number; ?>">
        <input type="hidden" name="title_pickup" value="<?php echo $pageTitle; ?>">
      </form>

      <div class="new-post">
        <div class="new-title">新着情報</div>

        <div class="carlist">
          <ul>

          <?php
          $args = array(
            'posts_per_page' => 3,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'pickup',
            'post_status' => 'publish'
          );

          $the_query = new WP_Query($args);

          // 記事一覧のループスタート
          if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post();

            $f_grade = get_field('f_grade'); // グレード
            $f_price_body = get_field('f_price_body'); // 車両本体価格(税込)
            $f_price_pay = get_field('f_price_pay'); // 支払総額(税込)
            $f_year = get_field('f_year'); // 年式
            $f_number = get_field('f_number'); // 車台番号
            $f_safety = get_field('f_safety'); // 車検有無
            $f_range = get_field('f_range'); // 走行距離
            $f_color = get_field('f_color'); // ボディカラー
            $f_option = get_field('f_option'); // オプション
            $f_photo_main = get_field('f_photo_main'); // 画像（メイン）
            $f_check = get_field('f_check'); // 整備
            $f_repair = get_field('f_repair'); // 修復歴
            $f_security = get_field('f_security'); // 保証
            $f_notes = get_field('f_notes'); // 補足
          ?>

          <li>
            <a href="<?php the_permalink(); ?>">
              <div class="carlist-inner">
                <div class="carlist-photo"><img src="<?php echo $f_photo_main ?>" alt="<?php the_title(); ?>"></div>
                <div class="carlist-title">
                  <p class="carlist-title-name"><?php the_title(); ?></p>
                  <p class="carlist-title-grade"><?php echo $f_grade; ?></p>
                </div>
                <dl class="carlist-price">
                  <dt>支払総額(税込)</dt>
                  <dd class="red"><span><?php echo $f_price_pay; ?></span>万円</dd>
                </dl>
                <div class="carlist-notes">
                  <dl>
                    <dt>車両価格</dt>
                    <dd><?php echo $f_price_body; ?>万円（税込）</dd>
                    <dt>諸費用</dt>
                    <dd>
                      <?php
                        // 支払総額 - 車両価格 = 諸費用
                        if ($f_price_pay != '' && $f_price_body != '') {
                          $overhead = floatval($f_price_pay) - floatval($f_price_body);
                          echo $overhead . '万円（税込）';
                        }
                      ?>
                    </dd>
                  </dl>
                  <p class="security">
                    <?php
                      $out_check = '';
                      if ($f_check == '定期点検整備つき') {
                        $out_check = 'あり';
                      } else {
                        $out_check = 'なし';
                      }

                      $out_security = '';
                      if ($f_security == '保証付') {
                        $out_security = '（' . $f_notes . '）';
                      }

                      $out_repair = '';
                      if ($f_repair == '修復歴あり') {
                        $out_repair = '／修復歴有り';
                      } else {
                        $out_repair = '／修復歴無し';
                      }
                      echo '法定整備' . $out_check . '／' . $f_security . $out_security . $out_repair;
                    ?>
                  </p>
                </div>
                <dl class="carlist-details">
                  <dt>年式</dt>
                  <dd><?php echo $f_year; ?></dd>
                  <dt>車台番号</dt>
                  <dd><?php echo $f_number; ?></dd>
                  <dt>車検有無</dt>
                  <dd><?php echo $f_safety; ?></dd>
                  <dt>走行距離</dt>
                  <dd><?php echo $f_range; ?></dd>
                  <dt>ボディカラー</dt>
                  <dd><?php echo $f_color; ?></dd>
                </dl>
                <p class="carlist-link"><span>詳しくみる</span></p>
              </div>
            </a>
          </li>

          <?php
            endwhile;
          endif;

          // 記事一覧のループ終わり
          wp_reset_postdata();
          ?>

          </ul>
        </div>
      </div>
    </div>
  </section>

  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/contact.php'); ?>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include/footer.php'); ?>
  <script src="/lib/pickup/js/style.js"></script>
</body>
</html>