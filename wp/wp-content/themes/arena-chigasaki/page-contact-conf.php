<?php
$setPath= "../";
$pageTitle = "お問い合わせ";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<?php
// session_start
function init_session_start() {
  if( session_status() !== PHP_SESSION_ACTIVE ) {
    session_start();
  }
}

add_action( 'init', 'init_session_start' );
// ライブラリのcommonを呼び出す - 全フォーム共通
require_once("lib/Mulgu/controller/Common.php");
// ライブラリのcontactを呼び出す - 対象フォーム用
require_once("lib/Mulgu/controller/Contact.php");
// common オブジェクト
$common = new Common();
// contact オブジェクト - パラメーター収めたりする
$contact = new Contact();
$obj = $contact->form();
$params = $obj->params;
if ( $obj->has_error ) {
  $errors = $obj->errors;
}
?>

<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>

<style type="text/css">
.mw_wp_form_confirm .conf-hidden {
  display: none;
}
</style>
</head>

<body class="contact">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <section class="keyVisual">
    <div class="titbox">
      <h2><span class="jp">お問い合わせ</span><br><span class="en">CONTACT</span></h2>
    </div>
  </section>

  <section class="con01">
    <div class="wrapper">
      <?php the_content(); ?>
    </div>
  </section>


  <?php require_once($setPath.'lib/include/contact.php'); ?>
  <?php require_once($setPath.'lib/include/footer.php'); ?>

  <script>
    // 送信ボタンsubmit
    jQuery('.js-send').on('click', function() {
      do_submit(this,'contact_form');
    });

    // 修正ボタンsubmit
    jQuery('.js-back').on('click', function() {
      document.contact_form.action='<?php echo $contact->path["form"]; ?>';
      do_submit(this,'contact_form');
      return false;
    });

    // 問い合わせ
    function do_submit(a,fname) {
      // $(a).addClass('loading').text('... 送信中');
      document[fname].submit();
      return false;
    }
  </script>
  
  <?php wp_footer(); ?>
</body>
</html>