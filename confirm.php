<?php
$setPath= "";
$pageTitle = "お問い合わせ";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<?php
session_start();
require_once(dirname(__FILE__)."/lib/Mulgu/controller/Common.php");
require_once(dirname(__FILE__)."/lib/Mulgu/controller/Contact.php");
$common = new Common();
$contact = new Contact();
$obj = $contact->confirm();
// params をなんやかんやする
$params = $obj->params;
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
</head>

<body class="home">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <section class="sec05" id="testdrive">
    <div class="wrapper">
      <h2 class="tit01">
        <span class="jp">試乗予約フォーム</span><br>
        <span class="en">FORM</span>
      </h2>


      <form class="contact-form form -confirm" name="contact_form" id="contact-form" action="<?php echo $contact->path["send"]; ?>" method="POST">
          <?php echo $common->mlg->tools->params_to_hidden($params); ?>


        <table>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>試乗車情報（希望車種）</span></th>
            <td class="contact-table-cont">
              <?php echo $params['type']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="required contact-table-ttl -any"><span>試乗希望日時</span></th>
            <td class="contact-table-cont">
              <?php echo $params['date']; ?>
              
              <?php echo $params['time']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>お名前</span></th>
            <td class="contact-table-cont">
              <?php echo $params['name']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>ふりがな</span></th>
            <td class="contact-table-cont">
              <?php echo $params['kana']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>日中連絡がつきやすい<br class="pc-on">お電話番号</span></th>
            <td class="contact-table-cont">
              <?php echo $params['phone']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>メールアドレス</span></th>
            <td class="contact-table-cont">
              <?php echo $params['email']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -any">郵便番号</th>
            <td class="contact-table-cont">
              〒<?php echo $params['code']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -any">ご住所</th>
            <td class="contact-table-cont">
              <?php echo $params['address']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -required required"><span>来店予定人数</span></th>
            <td class="contact-table-cont">
              <?php echo $params['people']; ?>
            </td>
          </tr>


          <tr class="contact-table-wrap">
            <th class="contact-table-ttl"><span>ご希望の連絡方法</span></th>
            <td class="contact-table-cont">
              <?php echo $params['contact_type']; ?>
            </td>
          </tr>

          
          <tr class="contact-table-wrap">
            <th class="contact-table-ttl -any">その他ご質問・ご要望</th>
            <td class="contact-table-cont">
              <?php echo nl2br($params['comment']); ?>
            </td>
          </tr>
        </table>


        <div class="contact-btn clearfix">
          <a href="javascript:void(0);" class="contact-btn-item -btn01 -confirm js-back">修正する</a>
          <a href="javascript:void(0)" class="contact-btn-item -btn01 -confirm js-send">この内容で送信する</a>
        </div>
      </form>
    </div>
  </section>



  <?php require_once($setPath.'lib/include/contact.php'); ?>
  <?php require_once($setPath.'lib/include/footer.php'); ?>

  <script>
    // 送信ボタンsubmit
    jQuery('.js-send').on('click', function() {
      do_submit(this,'contact_form');
    });

    // 修正ボタンsubmit
    jQuery('.js-back').on('click', function() {
      document.contact_form.action='<?php echo $contact->path["form"]; ?>';
      do_submit(this,'contact_form');
      return false;
    });

    // 問い合わせ
    function do_submit(a,fname) {
      // $(a).addClass('loading').text('... 送信中');
      document[fname].submit();
      return false;
    }
  </script>
  
</body>
</html>
